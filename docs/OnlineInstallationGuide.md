# Installation

## NodeJS
```bash
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## ComunicAPI
1. Clone the repository
2. Run `npm install`
3. Install webpack: `npm install webpack webpack-cli`

4. Build the project
```bash
node_modules/.bin/webpack
```

5. Create custom config: `cp config.json config.private.json` & customize it (do not forget to add the `proxy` and `force_clients_https` attributes if you need them!)

6. Adapt the following command to test the server: `/usr/bin/node /home/comunic/comunicapiv2/build/index.js /home/comunic/comunicapiv2/config.private.json`

7. Enable required modules in Apache:
```bash
sudo a2enmod proxy_http
sudo a2enmod proxy_wstunnel
```

8. Create Apache configuration, save it & restart Apache:
```conf
<VirtualHost *:443>
    ServerName api.communiquons.org

    ServerAdmin webmaster@localhost

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # Redirect user web socket
    RewriteEngine On
    RewriteCond %{REQUEST_URI} (.*)/ws$    [NC]
    RewriteRule /ws ws://localhost:3005/ws [P,L]


    ProxyPreserveHost On
    ProxyRequests off
    AllowEncodedSlashes NoDecode
    ProxyPass / http://localhost:3005/ nocanon
    ProxyPassReverse / http://localhost:3005/

    SSLEngine on
    SSLCertificateFile    /etc/letsencrypt/live/communiquons.org-0001/fullchain.pem
    SSLCertificateKeyFile  /etc/letsencrypt/live/communiquons.org-0001/privkey.pem
</VirtualHost>
```

9. Create systemd configuration (in `/etc/systemd/system/comunic.service`):
```conf
[Unit]
Description=Comunic API Server
After=syslog.target
After=network.target

[Service]
# Modify these two values ​​and uncomment them if you have
# repos with lots of files and get to HTTP error 500 because of that
###
# LimitMEMLOCK=infinity
# LimitNOFILE=65535
RestartSec=2s
Type=simple
User=comunic
Group=comunic
WorkingDirectory=/home/comunic
ExecStart=/usr/bin/node /home/comunic/comunicapiv2/build/index.js /home/comunic/comunicapiv2/config.private.json
Restart=always
Environment=USER=comunic 
HOME=/home/comunic

[Install]
WantedBy=multi-user.target
```

10. Enable new configuration:
```bash
sudo systemctl enable comunic.service
sudo systemctl start comunic.service
```