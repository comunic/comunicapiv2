# Account images Migration
May, 25 2020

**WARNING:** This migration breaks the compatibility with all the previous versions of Comunic. Starting with this update, Comunic API V2 & V3 are no longer backward compatible...

1. First, update database structure :
```sql
ALTER TABLE `utilisateurs` 
ADD COLUMN `account_image_path` VARCHAR(255) NOT NULL DEFAULT '' AFTER `lang`,
ADD COLUMN `account_image_visibility` VARCHAR(45) NOT NULL DEFAULT 'everyone' AFTER `account_image_path`;
```


2. Run the migration script:
```bash
node build/index.js config.json migration account_image_2020
```


3. In user data storage, delete the `avatars/adresse_avatars` directory.