const path = require("path");
const nodeExternals = require("webpack-node-externals");
const WebpackShellPlugin = require("webpack-shell-plugin");

const {
	NODE_ENV = 'production'
} = process.env;

module.exports = {
	entry: './src/main.ts',
	mode: NODE_ENV,
	target: 'node',
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'index.js'
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},

	module: {
		rules: [

			// Support for TS files
			{
				test: /\.ts$/,
				use: [
					'ts-loader'
				]
			}
		]
	},

	plugins: [

		// Allow live-reload
		new WebpackShellPlugin({
			onBuildEnd: ["node_modules/.bin/nodemon"]
		})

	],

	// Split dependencies and source code
	externals: [nodeExternals()],

	// Auto-watch in dev env
	watch: NODE_ENV === 'development'
}