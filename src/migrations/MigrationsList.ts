/**
 * List of currently available migrations
 * 
 * @author Pierre Hubert
 */

import { accountImageMigration } from "./AccountImageMigration";

interface Migration {
	id: string,
	func: () => Promise<void>
}

export const MigrationsList : Migration[] = [

	// Account image migration (files -> database, May 2020)
	{
		id: "account_image_2020",
		func: accountImageMigration
	}

]