/**
 * The Account image migration
 * 
 * In May 2020, I moved the references to account images
 * from simple txt files to the database, in order to 
 * improve both performances and beauty.
 * 
 * This migration (which should be run only once and immediatly
 * after API update) ensures that the database is filled with appropriate 
 * account image informations)
 * 
 * @author Pierre Hubert
 */

import { pathUserData } from "../utils/UserDataUtils";
import { readdirSync, readFileSync } from "fs";
import { AccountImageVisibilityLevel } from "../entities/User";
import { DatabaseHelper } from "../helpers/DatabaseHelper";
import { USER_TABLE } from "../helpers/AccountHelper";

const AVATARS_PATH = "avatars/";
const AVATARS_ADDRESSES_FILES = "avatars/adresse_avatars/";

enum OldAccountImageVisibilityLevel {
	FRIENDS = 1,
	COMUNIC_USERS = 2,
	EVERYONE = 3
}

const VisibilityLevelsMap = {};
VisibilityLevelsMap[OldAccountImageVisibilityLevel.FRIENDS] = AccountImageVisibilityLevel.FRIENDS
VisibilityLevelsMap[OldAccountImageVisibilityLevel.COMUNIC_USERS] = AccountImageVisibilityLevel.COMUNIC_USERS
VisibilityLevelsMap[OldAccountImageVisibilityLevel.EVERYONE] = AccountImageVisibilityLevel.EVERYONE


export async function accountImageMigration() {
	const pathFilesDatabase = pathUserData(AVATARS_ADDRESSES_FILES, true);

	// Process each file
	const list = readdirSync(pathFilesDatabase);

	for (const file of list) {
		
		// Ingore additional files
		if(file == ".htaccess" || file.endsWith(".php") || file.includes("jpg"))
			continue;

		const fileContent = readFileSync(pathFilesDatabase + file);
		const userID = Number(file.replace(".txt", "").replace("limit_view_", ""));

		// Account image path
		if(!file.includes("limit_view_")) {
			const newPath = AVATARS_PATH + fileContent;

			console.info("User " + userID + "\tAccount image: " + newPath);

			await DatabaseHelper.UpdateRows({
				table: USER_TABLE,
				where: {
					ID: userID
				},
				set: {
					account_image_path: newPath
				}
			})
		}

		// Account image visibility
		else {
			const newVisibililty = VisibilityLevelsMap[Number(fileContent)];

			console.info("User " + userID + "\tVisibility: " + newVisibililty);


			await DatabaseHelper.UpdateRows({
				table: USER_TABLE,
				where: {
					ID: userID
				},
				set: {
					account_image_visibility: newVisibililty
				}
			})
		}
	}
}