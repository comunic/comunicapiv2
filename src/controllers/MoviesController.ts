import { RequestHandler } from "../entities/RequestHandler";
import { MoviesHelper } from "../helpers/MoviesHelper";
import { Movie } from "../entities/Movie";

/**
 * Movies controller
 * 
 * @author Pierre HUBERT
 */

export class MoviesController {

	/**
	 * Get the list of movies of the user
	 * 
	 * @param h Request handler
	 */
	public static async GetList(h: RequestHandler) {
		const list = await MoviesHelper.GetListUser(h.getUserId());

		h.send(list.map((m) => this.MovieToAPI(m)));
	}

	/**
	 * Delete a movie
	 * 
	 * @param h Request handler
	 */
	public static async Delete(h: RequestHandler) {
		const movieID = await h.postMovieID("movieID");

		await MoviesHelper.Delete(await MoviesHelper.GetInfo(movieID));

		h.success();
	}

	/**
	 * Turn a movie into an API entry
	 * 
	 * @param m The movie to convert
	 */
	public static MovieToAPI(m: Movie): any {
		return {
			id: m.id,
			uri: m.uri,
			url: m.url,
			userID: m.userID,
			name: m.name,
			file_type: m.fileType,
			size: m.size
		}
	}
}