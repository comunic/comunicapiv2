import { LikesType, LikesHelper } from "../helpers/LikesHelper";
import { UserHelper } from "../helpers/UserHelper";
import { GroupsAccessLevel } from "../entities/Group";
import { UserLike } from "../entities/UserLike";
import { NotificationsHelper } from "../helpers/NotificationsHelper";
import { BaseRequestsHandler } from "../entities/BaseRequestsHandler";

/**
 * Likes controller
 * 
 * @author Pierre HUBERT
 */
export class LikesController {

	/**
	 * Update like status over an element
	 * 
	 * @param h Request handler
	 */
	public static async Update(h: BaseRequestsHandler) {
		const req_type = h.postString("type");
		const is_linking = h.postBool("like");

		let id: number;
		let type: LikesType;

		switch(req_type) {

			// In case of user
			case "user":
				id = await h.postUserId("id");
				type = LikesType.USER;

				if(!await UserHelper.CanSeeUserPage(h.getUserId(), id))
					h.error(401, "You cannot access this user information!")
				
				break;
			
			
			// In case of post
			case "post":
				id = await h.postPostIDWithAccess("id");
				type = LikesType.POST;
			
				// Delete any notification targetting this user about the post
				await NotificationsHelper.DeleteAllPostsNotificationsTargetingUser(h.getUserId(), id);
				
				break;
			
			
			// In case of comment
			case "comment":
				id = await h.postCommentIDWithAccess("id");
				type = LikesType.COMMENT;
				break;
			
			
			// In case of group
			case "group":
				id = await h.postGroupIDWithAccess("id", GroupsAccessLevel.VIEW_ACCESS);
				type = LikesType.GROUP;
				break;
			
			
			// Default case : throw an error
			default:
				throw Error("Unsupported like type : "+req_type+" !");

		}


		// Update like status
		await LikesHelper.Update(h.getUserId(), is_linking, id, type);


		h.success("");

	}

	/**
	 * Turn a user like entry into an API entry
	 * 
	 * @param l The like entry
	 */
	public static UserLikeToAPI(l: UserLike) : any {
		return {
			id: l.id,
			userID: l.userID,
			time_sent: l.timeSent,
			elem_type: l.elemType,
			elem_id: l.elemId
		}
	}
}