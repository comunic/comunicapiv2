import { RequestHandler } from "../entities/RequestHandler";
import { UserHelper } from "../helpers/UserHelper";
import { GroupsHelper } from "../helpers/GroupsHelper";

/**
 * Search controller
 * 
 * @author Pierre HUBERT
 */

export class SearchController {

	/**
	 * Search for user
	 * 
	 * @param h Request handler
	 */
	public static async SearchUser(h : RequestHandler) {
		
		// Get request
		const query = h.postString("query", 1);
		const limit = h.postInt("searchLimit", 5);

		const list = await UserHelper.SearchUser(query, limit);

		h.send(list);
	}

	/**
	 * Perform a global search (search both groups & users)
	 * 
	 * @param h Request handler
	 */
	public static async SearchGlobal(h: RequestHandler) {
		const query = h.postString("query", 1);
		const limit = 10;

		const results = [];

		// First, search for groups
		const groups = await GroupsHelper.SearchGroup(query, limit);
		groups.map((e) => ({
			kind: "group",
			id: e
		})).forEach((el) => results.push(el));

		// Then search for users
		const users = await UserHelper.SearchUser(query, limit);
		users.map((e) => ({
			kind: "user",
			id: e
		})).forEach((el) => results.push(el));
		
		h.send(results);
	}
}