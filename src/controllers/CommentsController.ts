import { RequestHandler } from "../entities/RequestHandler";
import { Comment } from "../entities/Comment";
import { LikesHelper, LikesType } from "../helpers/LikesHelper";
import { check_string_before_insert, removeHTMLNodes } from "../utils/StringUtils";
import { time } from "../utils/DateUtils";
import { CommentsHelper } from "../helpers/CommentsHelper";
import { NotifEventType } from "../entities/Notification";
import { NotificationsHelper } from "../helpers/NotificationsHelper";
import { AbstractUserConnectionContainer } from "../entities/UserConnectionContainer";
import { PostsHelper } from "../helpers/PostsHelper";

/**
 * Comments controller
 * 
 * @author Pierre HUBERT
 */

export class CommentsController {

	/**
	 * Create a new comment
	 * 
	 * @param h Request handler
	 */
	public static async Create(h: RequestHandler) {
		const postID = await h.postPostIDWithAccess("postID");

		// Check if the post allows comments
		if(!await PostsHelper.AllowCommentsOnPost(await PostsHelper.GetSingle(postID))) {
			h.error(401, "You can not create comments on this post!");
		}

		let content: string, image_path: string;

		// Check if an image was included in the request or not
		if(h.hasFile("image")) {
			content = this.GetCommentContent(h, "content", false);
			image_path = await h.savePostImage("image", "imgcommentaire", 700, 700);
		}

		else
			content = this.GetCommentContent(h, "content", true);

		const newComment = new Comment({
			id: -1,
			timeSent: time(),
			postID: postID,
			userID: h.getUserId(),
			content: content,
			imagePath: image_path
		});

		const commentID = await CommentsHelper.Create(newComment);

		// Create notifications
		await NotificationsHelper.CreatePostNotification(h.getUserId(), postID, NotifEventType.COMMENT_CREATED);

		// Delete any notifications targetting this user about the post
		await NotificationsHelper.DeleteAllPostsNotificationsTargetingUser(h.getUserId(), postID);

		h.send({success: true, commentID: commentID});
	}

	/**
	 * Get information about a single comment
	 * 
	 * @param h Request handler
	 */
	public static async GetSingle(h: RequestHandler) {
		const commentID = await h.postCommentIDWithAccess("commentID");
		const comment = await CommentsHelper.GetSingle(commentID);

		h.send(await this.CommentToAPI(h, comment))
	}

	/**
	 * Edit (update) a comment content
	 * 
	 * @param h Request handler
	 */
	public static async Edit(h: RequestHandler) {
		const commentID = await this.GetPostCommentIDWithFullAccess(h, "commentID");
		const newContent = this.GetCommentContent(h, "content", true);

		await CommentsHelper.Edit(commentID, newContent);

		h.success()
	}

	/**
	 * Delete a comment
	 * 
	 * @param h Request handler
	 */
	public static async Delete(h: RequestHandler) {
		const commentID = await this.GetPostCommentIDWithFullAccess(h, "commentID");

		await CommentsHelper.Delete(await CommentsHelper.GetSingle(commentID));

		h.success();
	}

	/**
	 * Get the content of a comment included in a POST field
	 * 
	 * @param h Request handler
	 * @param name The name of the post field
	 * @param need_check True if the comment must have valid content / false else
	 */
	private static GetCommentContent(h: RequestHandler, name: string, need_check = true) : string {
		const content = h.postContent(name, need_check ? 3 : 0);

		if(need_check && !check_string_before_insert(content))
			h.error(400, "Please check new comment content!");
		
		return content;
	}

	/**
	 * Get a comment ID on which current user has full access
	 * 
	 * @param h Request handler
	 * @param name The name of the POST field containing the comment ID
	 */
	private static async GetPostCommentIDWithFullAccess(h: RequestHandler, name: string) : Promise<number> {
		const commentID = h.postInt(name);

		if(!await CommentsHelper.IsOwner(h.getUserId(), commentID))
			h.error(401, "You are not the owner of this comment!");
		
		return commentID;
	}


	/**
	 * Turn a list of comment object into API entries
	 * 
	 * @param h Request handler
	 * @param l List of comments
	 */
	public static async CommentsToAPI(h: RequestHandler, l: Comment[]) : Promise<Array<any>> {
		const list = [];
		for (const comment of l) {
			list.push(await this.CommentToAPI(h, comment));
		}
		return list;
	}

	/**
	 * Turn a comment into an API object
	 * 
	 * @param h Request handler
	 * @param c Comment
	 * @param assumeNoLike Assume there are not any likes on this comment (false by default)
	 */
	public static async CommentToAPI(h: AbstractUserConnectionContainer, c: Comment, assumeNoLike: boolean = false) : Promise<any> {
		const data = {
			ID: c.id,
			userID: c.userID,
			postID: c.postID,
			time_sent: c.timeSent,
			content: c.content,
			img_path: c.hasImage ? c.imagePath : null,
			img_url: c.hasImage ? c.imageURL : null,
			likes: 0,
			userlike: false
		}

		// Check if we have to load like information
		if(!assumeNoLike) {
			data.likes = await LikesHelper.Count(c.id, LikesType.COMMENT);
			data.userlike = data.likes > 0 && h.signedIn ? await LikesHelper.IsLiking(h.getUserId(), c.id, LikesType.COMMENT) : false;
		}

		return data;
	}

}