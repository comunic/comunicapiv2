import { RequestHandler } from "../entities/RequestHandler";
import { UserHelper } from "../helpers/UserHelper";
import { User, UserPageStatus, AccountImageVisibilityLevel } from "../entities/User";
import { BackgroundImageHelper } from "../helpers/BackgroundImageHelper";
import { LikesHelper, LikesType } from "../helpers/LikesHelper";
import { FriendsHelper } from "../helpers/FriendsHelper";
import { CustomEmojisHelper } from "../helpers/CustomEmojisHelper";
import { SettingsController } from "./SettingsController";

/**
 * User information controller
 * 
 * @author Pierre HUBERT
 */

export class UserController {

	/**
	 * Get information about a single user
	 */
	public static async GetSingle(handler : RequestHandler) {
		const userID = handler.postInt("userID");
		const user = await UserHelper.GetUserInfo(userID);

		if(!user)
			handler.error(404, "Could not get user data!");
		
		handler.send(await this.UserToAPI(user, handler));
	}

	/**
	 * Get information about multiple users
	 */
	public static async GetMultiple(handler : RequestHandler) {
		
		let list = {};
		const IDs = handler.postNumbersList("usersID");

		for (const id of IDs) {
			if(list.hasOwnProperty(id))
				continue;
			
			const user = await UserHelper.GetUserInfo(id);

			if(!user)
				handler.error(404, "One user was not found!");
			
			list[id] = await this.UserToAPI(user, handler);
		}

		handler.send(list);
	}

	/**
	 * Get advanced information about a user
	 * 
	 * @param h Request handler
	 */
	public static async GetAdvancedInfo(h: RequestHandler) {
		const userID = await h.postUserId("userID");

		if(!await UserHelper.CanSeeUserPage(h.optionnalUserID, userID))
			h.error(401, "You are not allowed to access these information!");
		
		const user = await UserHelper.GetUserInfo(userID);
		const result = this.UserToAPI(user, h, true);

		h.send(await result);
	}


	public static async UserToAPI(user : User, h: RequestHandler, advanced: boolean = false) : Promise<Object> {
		const info = {
			"userID": user.id,
			"firstName": user.firstName,
			"lastName": user.lastName,
			"publicPage": user.pageStatus == UserPageStatus.PUBLIC,
			"openPage": user.pageStatus == UserPageStatus.OPEN,
			"virtualDirectory": user.hasVirtualDirectory ? user.virtualDirectory : "",
			"accountImage": await this.GetAccountImageURL(user, h),
			"customEmojis": (await CustomEmojisHelper.GetListUser(user.id)).map(SettingsController.CustomEmojiToAPI)
		};

		if(advanced) {
			info["friend_list_public"] = user.friendsListPublic;
			info["personnalWebsite"] = user.hasWebsite ? user.personnalWebsite : "";
			info["publicNote"] = user.hasPublicNote ? user.publicNote : "";
			info["noCommentOnHisPage"] = user.blockComments;
			info["allowPostFromFriendOnHisPage"] = user.allowPostsFromFriends;
			info["account_creation_time"] = user.timeCreate;
			info["backgroundImage"] = BackgroundImageHelper.GetURL(user.id);
			info["number_friends"] = user.friendsListPublic || user.id == h.optionnalUserID ? await FriendsHelper.CountForUser(user.id) : 0
			
			info["pageLikes"] = await LikesHelper.Count(user.id, LikesType.USER);
			info["user_like_page"] = h.signedIn ? await LikesHelper.IsLiking(h.getUserId(), user.id, LikesType.USER) : false;

			info["can_post_texts"] = h.signedIn ? await UserHelper.CanCreatePosts(h.getUserId(), user.id) : false;
		}

		return info;
	}

	private static async GetAccountImageURL(user : User, handler: RequestHandler) {
		
		if(user.accountImageVisibilityLevel == AccountImageVisibilityLevel.EVERYONE 
			|| (handler.signedIn && handler.getUserId() == user.id))
			return user.accountImageURL;

		if(!handler.signedIn)
			return User.errorAccountImageURL;

		if(user.accountImageVisibilityLevel == AccountImageVisibilityLevel.COMUNIC_USERS
			|| await FriendsHelper.AreFriend(user.id, handler.getUserId())) {
			return user.accountImageURL;
		}
		
		return User.errorAccountImageURL;
	}
}