import { Survey, SurveyChoice } from "../entities/Survey";
import { RequestHandler } from "../entities/RequestHandler";
import { SurveyHelper, MAXIMUM_NUMBER_SURVEY_CHOICES } from "../helpers/SurveyHelper";
import { SurveyResponse } from "../entities/SurveyResponse";
import { PostAccessLevel } from "../entities/Post";

/**
 * Survey controller
 * 
 * @author Pierre HUBERT
 */

export class SurveyController {

	/**
	 * Get information about a single survey
	 * 
	 * @param h Request handler
	 */
	public static async GetInfoSingle(h: RequestHandler) {
		const surveyID = await this.PostSurveyIDFromPostID(h, "postID");
		const survey = await SurveyHelper.GetInfoBySurveyID(surveyID);

		h.send(await this.SurveyToAPI(h, survey));
	}

	/**
	 * Send the response to a survey to the server
	 * 
	 * @param h Request handler
	 */
	public static async SendResponse(h: RequestHandler) {
		const surveyID = await this.PostSurveyIDFromPostID(h, "postID");
		const choiceID = h.postInt("choiceID");

		await SurveyHelper.CancelResponse(h.getUserId(), surveyID);

		if(!await SurveyHelper.ChoiceExists(surveyID, choiceID))
			h.error(404, "Choice not found for this survey!");
		
		await SurveyHelper.SendResponse(h.getUserId(), surveyID, choiceID);

		h.success();
	}

	/**
	 * Cancel the repsonse to a request
	 * 
	 * @param h Request handler
	 */
	public static async CancelResponse(h: RequestHandler) {
		const surveyID = await this.PostSurveyIDFromPostID(h, "postID");
		
		await SurveyHelper.CancelResponse(h.getUserId(), surveyID);

		h.success();
	}

	/**
	 * Create a new choice for this survey
	 * 
	 * @param h Request handler
	 */
	public static async CreateNewChoice(h: RequestHandler) {

		const surveyID = await this.PostSurveyIDFromPostID(h, "postID");
		const newChoice = h.postString("choice");

		// Check if the survey allow new choices
		const survey = await SurveyHelper.GetInfoBySurveyID(surveyID);
		if(!survey.allowNewChoices)
			h.error(401, "It is not possible to create new choices for this survey!");

		// Check if we are not creating a duplicate
		if(survey.choices.find((c) => c.name.toLowerCase() == newChoice.toLowerCase()) != undefined)
			h.error(401, "This choice already exists!");

		// Create the choice
		await SurveyHelper.CreateChoice(surveyID, newChoice);

		// Auto-block creation of new choices if limit is reached
		if(survey.choices.length + 1 >= MAXIMUM_NUMBER_SURVEY_CHOICES)
			await SurveyHelper.BlockNewChoicesCreation(surveyID);

		h.success();
	}

	/**
	 * Block the creation of new survey choices
	 * 
	 * @param h Request handler
	 */
	public static async BlockNewChoicesCreation(h: RequestHandler) {
		const surveyID = await this.PostSurveyIDFromPostID(h, "postID", PostAccessLevel.FULL_ACCESS);

		await SurveyHelper.BlockNewChoicesCreation(surveyID);

		h.success();
	}


	/**
	 * Turn a survey into an API entry
	 * 
	 * @param h Request handler
	 * @param survey The survey
	 */
	public static async SurveyToAPI(h: RequestHandler, survey: Survey) : Promise<any> {
		let data  = {
			ID: survey.id,
			userID: survey.userID,
			postID: survey.postID,
			creation_time: survey.timeCreate,
			question: survey.question,
			user_choice: -1,
			choices: {},
			allowNewChoices: survey.allowNewChoices,
		}

		survey.choices.forEach((c) => data.choices[c.id.toString()] = this.SurveyChoiceToAPI(c))

		if(h.signedIn)
			data.user_choice = await SurveyHelper.GetUserChoice(survey.id, h.getUserId());

		return data;
	}

	/**
	 * Get the ID of a survey from a POST id
	 * 
	 * @param h Request handler
	 * @param field The name of the POST field containing the
	 * POST id
	 * @param minLevel Minimal access level to the post
	 */
	private static async PostSurveyIDFromPostID(h: RequestHandler, field: string, 
		minLevel?: PostAccessLevel) : Promise<number> {	
		const postID = await h.postPostIDWithAccess(field, minLevel);
		return await SurveyHelper.GetID(postID);
	}

	/**
	 * Turn a survey choice into an API entry
	 * 
	 * @param c The choice
	 */
	private static SurveyChoiceToAPI(c: SurveyChoice) {
		return {
			choiceID: c.id,
			name: c.name,
			responses: c.count
		}
	}

	/**
	 * Turn a {SurveyResponse} object into an API entry
	 * 
	 * @param r The survey response
	 */
	public static SurveyResponseToAPI(r: SurveyResponse) : any {
		return {
			id: r.id,
			time_sent: r.timeSent,
			userID: r.userID,
			surveyID: r.surveyID,
			choiceID: r.choiceID
		}
	}
}