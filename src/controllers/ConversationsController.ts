import { RequestHandler } from "../entities/RequestHandler";
import { ConversationsHelper } from "../helpers/ConversationsHelper";
import { Conversation, BaseConversation } from "../entities/Conversation";
import { UserHelper } from "../helpers/UserHelper";
import { removeHTMLNodes } from "../utils/StringUtils";
import { ConversationMessage } from "../entities/ConversationMessage";
import { UnreadConversation } from "../entities/UnreadConversation";
import { CallsHelper } from "../helpers/CallsHelper";
import { CallsController } from "./CallsController";

/**
 * Conversations controller
 * 
 * @author Pierre HUBERT
 */

export class ConversationsController {

	/**
	 * Create a new conversation
	 * 
	 * @param h Request handler
	 */
	public static async CreateConversation(h : RequestHandler) {

		const name = h.postString("name");

		const members = h.postNumbersSet("users");
		
		// Check if the users exists
		for (const userID of members) {
			if(!await UserHelper.Exists(userID))
				h.error(404, "User " + userID + " not found!");
		}

		if(!members.has(h.getUserId()))
			members.add(h.getUserId());

		const conv : BaseConversation = {
			ownerID: h.getUserId(),
			name: name == "false" ? "" : removeHTMLNodes(name),
			following: h.postBool("follow"),
			members: members,
			canEveryoneAddMembers: h.postBool("canEveryoneAddMembers", true),
		}

		const convID = await ConversationsHelper.Create(conv);

		// Success
		h.send({
			conversationID: convID,
			success: "The conversation was successfully created!"
		})
	}

	/**
	 * Get the list of conversations of the user
	 * 
	 * @param handler 
	 */
	public static async GetList(handler: RequestHandler) {
		const list = await ConversationsHelper.GetListUser(handler.getUserId());
		handler.send(list.map(c => this.ConversationToAPI(c)));
	}

	/**
	 * Get information about a single conversation
	 * 
	 * @param handler 
	 */
	public static async GetInfoSingle(handler: RequestHandler) {
		const conversationID = await handler.postConversationId("conversationID");
		const conv = await ConversationsHelper.GetSingle(conversationID, handler.getUserId());

		if(!conv)
			throw Error("Could not get information about the conversation (but it should have been found though) !!");
		
		handler.send(this.ConversationToAPI(conv));
	}

	/**
	 * Update conversation settings
	 * 
	 * @param h Request handler
	 */
	public static async UpdateSettings(h: RequestHandler) : Promise<void> {
		const convID = await h.postConversationId("conversationID");
		const isUserModerator = await ConversationsHelper.IsUserModerator(h.getUserId(), convID);

		// Update following state, if required
		if(h.hasPostParameter("following")) {
			await ConversationsHelper.SetFollowing(
				h.getUserId(),
				convID,
				h.postBool("following")
			);
		}
	
		// Update members list
		if(h.hasPostParameter("members")) {
			const members = await h.postUsersIDSet("members");
			const canEveryoneAddMembers = await ConversationsHelper.CanEveryoneAddMembers(convID);

			if(!isUserModerator && !canEveryoneAddMembers)
				h.error(401, "You can not update the list of members of this conversation!");

			// Make sure current user is on the list
			if(!members.has(h.getUserId()))
				members.add(h.getUserId());
			
			await ConversationsHelper.SetMembers(convID, members, isUserModerator);
		}

		// Change moderator settings
		if(h.hasPostParameter("name") || h.hasPostParameter("canEveryoneAddMembers")) {
			
			// Check if user is the moderator of the conversation
			if(!isUserModerator)
				h.error(401, "You are not allowed to perform changes on this conversation !");
			
			// Update conversation name (if required)
			if(h.hasPostParameter("name")) {
				const name = h.postString("name", 0);
				await ConversationsHelper.SetName(convID, name == "false" ? "" : removeHTMLNodes(name));
			}

			// Update "canEveryoneAddMembers" parameter
			if(h.hasPostParameter("canEveryoneAddMembers")) {
				await ConversationsHelper.SetCanEveryoneAddMembers(convID, h.postBool("canEveryoneAddMembers"));
			}

		}

		h.success("Conversation information successfully updated!");
	}

	/**
	 * Find, optionally create and return the ID of a privat conversation
	 * 
	 * @param h Request handler
	 */
	public static async FindPrivate(h: RequestHandler) {
		const otherUser = await h.postUserId("otherUser");
		const allowCreate = h.postBool("allowCreate", false);

		// Query the database
		const list = await ConversationsHelper.FindPrivate(h.getUserId(), otherUser);

		if(list.length == 0) {

			if(!allowCreate)
				h.error(404, "Not any private conversation was found. The server was not allowed to create a new one...");
			
			// Create the conversation
			const convID = await ConversationsHelper.Create({
				ownerID: h.getUserId(),
				name: "",
				following: true,
				members: new Set([h.getUserId(), otherUser]),
				canEveryoneAddMembers: true
			});

			list.push(convID);
		}


		h.send({"conversationsID": list});
	}

	/**
	 * Refresh current user conversations
	 * 
	 * @param h Request handler
	 * @deprecated Only ComunicWeb was using this method until
	 * the introduction of WebSockets...
	 */
	public static async RefreshList(h: RequestHandler) {

		const list = {};

		// Check for new conversations
		if(h.hasPostParameter("newConversations")) {
			for(const convID of h.postNumbersSet("newConversations", 0)) {
				if(!await ConversationsHelper.DoesUsersBelongsTo(h.getUserId(), convID))
					h.error(401, "You are not allowed to fetch the messages of this conversation ("+convID+")!");
				
				list["conversation-" + convID] = (await ConversationsHelper.GetLastMessages(convID, 10))
					.map(e => this.ConversationMessageToAPI(e));
				
				// Mark the user has seen the messages
				await ConversationsHelper.MarkUserSeen(convID, h.getUserId());
			}
		}

		// Check for refresh on some conversations
		if(h.hasPostParameter("toRefresh")) {
			const toRefresh = h.postJSON("toRefresh");

			for (const key in toRefresh) {
				if (toRefresh.hasOwnProperty(key)) {
					const element = toRefresh[key];
					
					// Extract conversation ID
					if(!key.startsWith("conversation-"))
						h.error(400, "Entries of 'toRefresh' should start with 'conversation-' !");
					const convID = Number.parseInt(key.replace("conversation-", ""));

					// Extract last message ID
					if(!element.hasOwnProperty("last_message_id"))
						h.error(400, "Missing last_message_id for conversation " + convID + "!");
					const lastMessageID = Number.parseInt(element.last_message_id);

					// Check user rights
					if(!await ConversationsHelper.DoesUsersBelongsTo(h.getUserId(), convID))
						h.error(401, "You are not allowed to fetch the messages of this conversation ("+convID+")!");
					
					// Get the messages
					list["conversation-" + convID] = (await ConversationsHelper.GetNewMessages(convID, lastMessageID))
						.map(e => this.ConversationMessageToAPI(e));

					// Mark the user has seen the messages
					await ConversationsHelper.MarkUserSeen(convID, h.getUserId());
				}
			}
		}

		h.send(list);
	}

	/**
	 * Refresh the messages of a single conversation
	 * 
	 * @param h Request handler
	 */
	public static async RefreshSingleConversation(h: RequestHandler) {
		const convID = await h.postConversationId("conversationID");
		const lastMessageID = h.postInt("last_message_id");

		const messages = lastMessageID == 0 ?
			
			// Get latest messages (the user has no message yet)
			await ConversationsHelper.GetLastMessages(convID, 10) :

			// Get new messages
			await ConversationsHelper.GetNewMessages(convID, lastMessageID);
		
		// Specify the user has seen the last message
		await ConversationsHelper.MarkUserSeen(convID, h.getUserId());

		h.send(messages.map(e => this.ConversationMessageToAPI(e)));
	}

	/**
	 * Send a new message
	 * 
	 * @param h Request handler
	 */
	public static async SendMessage(h: RequestHandler) {
		const convID = await h.postConversationId("conversationID");
		const message = removeHTMLNodes(h.postString("message", 0));
		let imagePath = "";

		// Check for image
		if(h.hasFile("image")){
			// Try to save image
			imagePath = await h.savePostImage("image", "conversations", 1200, 1200);
		}
		
		else if(message.length < 3)
			h.error(401, "Message is empty!");
		
		// Send message
		await ConversationsHelper.SendMessage({
			userID: h.getUserId(),
			convID: convID,
			message: message,
			imagePath: imagePath
		});
		
		h.success("Conversation message was sent!");
	}

	/**
	 * Get the older messages of a conversation
	 * 
	 * @param h Request handler
	 */
	public static async GetOlderMessages(h: RequestHandler) {
		const convID = await h.postConversationId("conversationID");
		const maxID = h.postInt("oldest_message_id") - 1;
		
		let limit = h.postInt("limit");
		if(limit < 1) limit = 1;
		else if(limit > 30) limit = 30;

		// Get the list of messages
		const messages = await ConversationsHelper.GetOlderMessage(convID, maxID, limit);

		h.send(messages.map(e => this.ConversationMessageToAPI(e)));
	}

	/**
	 * Get the number of unread conversations for the user
	 * 
	 * @param h Request handler
	 */
	public static async CountUnreadForUser(h: RequestHandler) {
		h.send({
			nb_unread: await ConversationsHelper.CountUnreadForUser(h.getUserId())
		})
	}

	/**
	 * Get the list of unread conversations of the user
	 * 
	 * @param h Request handler
	 */
	public static async GetListUnread(h: RequestHandler) {
		const list = await ConversationsHelper.GetListUnread(h.getUserId());

		h.send(list.map(e => this.UnreadConversationToAPI(e)));
	}

	/**
	 * Request a conversation
	 * 
	 * @param h Request handler
	 */
	public static async DeleteConversation(h: RequestHandler) {
		const convID = await h.postConversationId("conversationID");

		await ConversationsHelper.RemoveUserFromConversation(h.getUserId(), convID);

		h.success("The conversation has been deleted.");
	}

	/**
	 * Update the content of a conversation message
	 * 
	 * @param h Request handler
	 */
	public static async UpdateMessage(h: RequestHandler) {
		const messageID = h.postInt("messageID");
		const newContent = h.postString("content");

		if(newContent.length < 3)
			h.error(401, "Invalid new message content!");
		
		// Check out whether the user own the message or not
		if(!await ConversationsHelper.IsUserMessageOwner(h.getUserId(), messageID))
			h.error(401, "You do not own this conversation message!");

		await ConversationsHelper.UpdateMessageContent(messageID, newContent);

		h.success("Conversation message content successfully update");
	}

	/**
	 * Delete a conversation message
	 * 
	 * @param h Request handler
	 */
	public static async DeleteMessage(h: RequestHandler) {
		const messageID = h.postInt("messageID");

		if(!await ConversationsHelper.IsUserMessageOwner(h.getUserId(), messageID))
			h.error(401, "You do not own this conversation message!");
		
		await ConversationsHelper.DeleteMessageById(messageID);

		h.success("Conversation message has been successfully deleted!");
	}

	/**
	 * Turn a conversation object into an API entry
	 * 
	 * @param c 
	 */
	public static ConversationToAPI(c : Conversation) : any {
		return {
			ID: c.id,
			ID_owner: c.ownerID,
			last_active: c.lastActive,
			name: c.name.length > 0 ? c.name : false,
			following: c.following ? 1 : 0,
			saw_last_message: c.sawLastMessage ? 1 : 0,
			members: [...c.members],
			canEveryoneAddMembers: c.canEveryoneAddMembers,
			can_have_call: CallsHelper.CanHaveCall(c),
			can_have_video_call: CallsHelper.CanHaveVideoCAll(c),
			has_call_now: CallsController.IsHavingCall(c.id)
		};
	}

	/**
	 * Turn a conversation message into an API object
	 * 
	 * @param c Information about the conversation
	 */
	public static ConversationMessageToAPI(c: ConversationMessage) : any {
		return {
			ID: c.id,
			convID: c.convID,
			ID_user: c.userID,
			time_insert: c.timeSent,
			message: c.message,
			image_path: c.hasImage ? c.imageURL : null
		};
	}

	/**
	 * Turn an UnreadConversation object into an API entry
	 * 
	 * @param c Target conversation
	 */
	private static UnreadConversationToAPI(c: UnreadConversation): any {
		return {
			id: c.id,
			conv_name: c.name,
			last_active: c.lastActive,
			userID: c.userID,
			message: c.message
		}
	}
}