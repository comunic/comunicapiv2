/**
 * Settings controller
 * 
 * @author Pierre HUBERT
 */

import { RequestHandler } from "../entities/RequestHandler";
import { UserHelper } from "../helpers/UserHelper";
import { GeneralSettings, UserPageStatus, SupportedLanguages, LangSettings, SecuritySettings } from "../entities/User";
import { removeHTMLNodes, checkURL, fixEncoding } from "../utils/StringUtils";
import { checkVirtualDirectoryAvailability, VirtualDirType } from "../utils/VirtualDirsUtils";
import { AccountHelper } from "../helpers/AccountHelper";
import { AccountImageVisibilityLevel } from "../entities/User";
import { AccountImageHelper } from "../helpers/AccountImageHelper";
import { findKey } from "../utils/ArrayUtils";
import { CustomEmojisHelper } from "../helpers/CustomEmojisHelper";
import { CustomEmoji } from "../entities/CustomEmoji";

/**
 * API account image visibility levels
 */
const ACCOUNT_IMAGE_VISIBLITY_LEVELS = {};
ACCOUNT_IMAGE_VISIBLITY_LEVELS[AccountImageVisibilityLevel.EVERYONE] = "open";
ACCOUNT_IMAGE_VISIBLITY_LEVELS[AccountImageVisibilityLevel.COMUNIC_USERS] = "public";
ACCOUNT_IMAGE_VISIBLITY_LEVELS[AccountImageVisibilityLevel.FRIENDS] = "friends";


export class SettingsController {

	/**
	 * Get general account settings
	 * 
	 * @param h Request handler
	 */
	public static async GetGeneral(h: RequestHandler) {

		const userInfo = await UserHelper.GetUserInfo(h.getUserId());

		h.send({
			id: userInfo.id,
			email: userInfo.email,
			firstName: userInfo.firstName,
			lastName: userInfo.lastName,
			is_public: userInfo.isPublic,
			is_open: userInfo.isOpen,
			allow_comments: !userInfo.blockComments,
			allow_posts_from_friends: userInfo.allowPostsFromFriends,
			allow_comunic_mails: userInfo.allowMails,
			public_friends_list: userInfo.friendsListPublic,
			virtual_directory: userInfo.virtualDirectory,
			personnal_website: userInfo.personnalWebsite,
			publicNote: userInfo.publicNote,
		})
	}

	/**
	 * Update (set) general account settings
	 * 
	 * @param h Request handler
	 */
	public static async SetGeneral(h: RequestHandler) {

		// Determine page status
		const pageStatus = h.postBool("isPublic") ? (
			h.postBool("isOpen") ? UserPageStatus.OPEN : UserPageStatus.PUBLIC
		) : UserPageStatus.PRIVATE;

		// Check personnal website
		const personnalWebsite = h.postString("personnalWebsite", 0);
		if(personnalWebsite.length > 0 && !checkURL(personnalWebsite))
			h.error(401, "Invalid personnal website supplied!");
		
		// Check virtual directory
		let virtualDirectory = h.postString("virtualDirectory", 0);
		if(virtualDirectory.length > 0) {
			virtualDirectory = h.postVirtualDirectory("virtualDirectory");

			if(!await checkVirtualDirectoryAvailability(virtualDirectory, h.getUserId(), VirtualDirType.USER))
				h.error(401, "The specified virtual directory is not available!");
		}

		// Construct new settings object
		const newSettings : GeneralSettings = {
			id: h.getUserId(),
			firstName: removeHTMLNodes(h.postString("firstName", 3)),
			lastName: removeHTMLNodes(h.postString("lastName", 3)),
			pageStatus: pageStatus,
			blockComments: !h.postBool("allowComments"),
			allowPostsFromFriends: h.postBool("allowPostsFromFriends"),
			friendsListPublic: h.postBool("publicFriendsList"),
			personnalWebsite: personnalWebsite,
			virtualDirectory: virtualDirectory,
			allowMails: h.postBool("allow_comunic_mails"),
			publicNote: removeHTMLNodes(h.postString("publicNote", 0))
		};

		await AccountHelper.SetGeneral(newSettings);

		h.success();

	}


	/**
	 * Check the availablity of a virtual directory for a user
	 * 
	 * @param h Request handler
	 */
	public static async CheckDirectoryAvailability(h: RequestHandler) {
		const directory = h.postVirtualDirectory("directory");

		if(!await checkVirtualDirectoryAvailability(directory, h.getUserId(), VirtualDirType.USER))
			h.error(401, "The specified directory is unavailable!");
		
		h.success("The directory is available!");
	}

	/**
	 * Get language settings
	 * 
	 * @param h Request handler
	 */
	public static async GetLanguage(h: RequestHandler) {
		const userInfo = await UserHelper.GetUserInfo(h.getUserId());

		h.send({
			lang: userInfo.lang
		});
	}

	/**
	 * Update (set) language settings
	 * 
	 * @param h Request handler
	 */
	public static async SetLanguage(h: RequestHandler) {
		const lang = h.postString("lang", 2);

		if(!SupportedLanguages.includes(lang))
			h.error(401, "Language not supported!");
		
		
		const newSettings: LangSettings = {
			id: h.getUserId(),
			lang: lang
		}

		await AccountHelper.SetLanguageSettings(newSettings);

		h.success();
	}

	/**
	 * Get security settings
	 * 
	 * @param h Request handler
	 */
	public static async GetSecurity(h: RequestHandler) {
		await h.needUserPostPassword("password");

		const userInfo = await UserHelper.GetUserInfo(h.getUserId());

		h.send({
			id: userInfo.id,
			security_question_1: userInfo.hasSecurityQuestion1 ? userInfo.security_question_1 : "",
			security_answer_1: userInfo.hasSecurityAnswer1 ? userInfo.security_answer_1 : "",
			security_question_2: userInfo.hasSecurityQuestion2 ? userInfo.security_question_2 : "",
			security_answer_2: userInfo.hasSecurityAnswer2 ? userInfo.security_answer_2 : "",
		});
	}

	/**
	 * Set security settings
	 * 
	 * @param h Request handler
	 */
	public static async SetSecurity(h: RequestHandler) {
		await h.needUserPostPassword("password");

		const newSettings: SecuritySettings = {
			id: h.getUserId(),
			security_question_1: removeHTMLNodes(h.postString("security_question_1", 0)),
			security_answer_1: removeHTMLNodes(h.postString("security_answer_1", 0)),
			security_question_2: removeHTMLNodes(h.postString("security_question_2", 0)),
			security_answer_2: removeHTMLNodes(h.postString("security_answer_2", 0))
		}

		await AccountHelper.SetSecuritySettings(newSettings);

		h.success();
	}

	/**
	 * Check current user password
	 * 
	 * @param h Request handler
	 */
	public static async CheckUserPassword(h: RequestHandler) {
		
		await h.needUserPostPassword("password");

		h.success();
	}

	/**
	 * Update user password
	 * 
	 * @param h Request handler
	 */
	public static async UpdatePassword(h: RequestHandler) {
		await h.needUserPostPassword("oldPassword");

		const newPassword = h.postString("newPassword");

		await AccountHelper.ChangePassword(h.getUserId(), newPassword);

		h.success();
	}

	/**
	 * Get account image settings
	 * 
	 * @param h Request handler
	 */
	public static async GetAccountImageSettings(h: RequestHandler) {
		const user = await UserHelper.GetUserInfo(h.getUserId());

		h.send({
			has_image: user.hasAccountImage,
			image_url: user.accountImageURL,
			visibility: ACCOUNT_IMAGE_VISIBLITY_LEVELS[user.accountImageVisibilityLevel]
		});
	}

	/**
	 * Upload new account image
	 * 
	 * @param h Request handler
	 */
	public static async UploadAccountImage(h: RequestHandler) {
		if(!h.hasFile("picture"))
			h.error(400, "An error occured while receiving the image !")
		
		const uri = await h.savePostImage("picture", "avatars", 800, 800);

		await AccountImageHelper.Set(h.getUserId(), uri);

		h.success();
	}

	/**
	 * Delete account account image
	 * 
	 * @param h Request handler
	 */
	public static async DeleteAccountImage(h: RequestHandler) {
		await AccountImageHelper.Delete(h.getUserId());
		h.success();
	}

	/**
	 * Change account image visibility level
	 * 
	 * @param h Request handler
	 */
	public static async SetAccountImageVisibility(h: RequestHandler) {
		
		const visibilityLevel = findKey(ACCOUNT_IMAGE_VISIBLITY_LEVELS, h.postString("visibility"));
		if(visibilityLevel == null)
			h.error(400, "Account image visibility level not understood!");
		
		await AccountImageHelper.SetVisibilityLevel(
			h.getUserId(), <AccountImageVisibilityLevel>String(visibilityLevel));

		h.success();
	}

	/**
	 * Upload a custom emoji to the server
	 * 
	 * @param h Request handler
	 */
	public static async UploadCustomEmoji(h: RequestHandler) {
		const shortcut = h.postEmojiShortcut("shortcut");

		// Check if the user has already a shortcut with the same name
		if(await CustomEmojisHelper.HasUserSimilarShortcut(h.getUserId(), shortcut)) 
			h.error(401, "A custom emoji with the same shortcut is already defined!")
		

		const path = await h.savePostImage("image", "custom_emojies", 72, 72);

		// Create the emoji
		const emojiID = await CustomEmojisHelper.Insert(new CustomEmoji({
			id: -1,
			userID: h.getUserId(),
			shortcut: shortcut,
			path: path
		}))

		h.send({
			emojiID: emojiID 
		})

	}

	/**
	 * Delete a custom emoji of the user
	 * 
	 * @param h Request handler
	 */
	public static async DeleteCustomEmoji(h: RequestHandler) {
		const emoji = await h.postEmojiID("emojiID");

		await CustomEmojisHelper.Delete(emoji);

		h.success()
	}

	/**
	 * Convert an emoji into an API entry
	 * 
	 * @param c The emoji to convert
	 */
	public static CustomEmojiToAPI(c: CustomEmoji) : any {
		return {
			id: c.id,
			userID: c.userID,
			shortcut: c.shortcut,

			// Legacy (deprecated)
			shorcut: c.shortcut,

			url: c.url
		}
	}
}