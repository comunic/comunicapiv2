import { Friend } from "../entities/Friend";
import { RequestHandler } from "../entities/RequestHandler";
import { FriendsHelper } from "../helpers/FriendsHelper";
import { UserHelper } from "../helpers/UserHelper";
import { NotifEventType } from "../entities/Notification";
import { NotificationsHelper } from "../helpers/NotificationsHelper";
import { AccountHelper } from "../helpers/AccountHelper";
import { UserWebSocketController } from "./UserWebSocketController";
import { time } from "../utils/DateUtils";

/**
 * Friends controller
 * 
 * @author Pierre HUBERT
 */

export class FriendsController {

	/**
	 * Get the list of friends of a user
	 * 
	 * @param h Request handler
	 */
	public static async GetList(h: RequestHandler) {
		//const returnAllInfo = h.postBool("complete", false);
		const list = await FriendsHelper.GetList(h.getUserId());

		// Update user activity (if allowed)
		if(!h.postBool("incognito", false))
			await AccountHelper.UpdateLastActivity(h.getUserId())

		h.send(list.map((f) => this.FriendToAPI(f)));
	}

	/**
	 * Get another user friends list
	 * 
	 * @param h Request handler
	 */
	public static async GetOtherUserList(h: RequestHandler) {
		const userID = await h.postUserId("userID");

		if(!await UserHelper.CanSeeUserPage(h.optionnalUserID, userID))
			h.error(401, "You are not allowed to access these information!");
		
		if(!await UserHelper.IsUserFriendListPublic(userID))
			h.error(401, "The friends list of the user is not public!");
		
		const friends = await FriendsHelper.GetList(userID);

		h.send(friends.map((f) => f.friendID));
	}

	/**
	 * Get single friendship information
	 * 
	 * @param h Request handler
	 */
	public static async GetSingleFrienshipInfo(h: RequestHandler) {
		const friendID = await h.postUserId("friendID");

		const info = await FriendsHelper.GetSingleInfo(h.getUserId(), friendID);

		if(info == null)
			h.error(404, "Requested frienship not found!");

		h.send(this.FriendToAPI(info));
	}

	/**
	 * Get a friendship status
	 * 
	 * @param h Request handler
	 */
	public static async GetStatus(h: RequestHandler) {
		const friendID = await h.postUserId("friendID");

		let response = {
			are_friend: false,
			sent_request: false,
			received_request: false,
			following: false
		}
	
		response.are_friend = await FriendsHelper.AreFriend(h.getUserId(), friendID);

		if(!response.are_friend) {
			response.sent_request = await FriendsHelper.SentRequest(h.getUserId(), friendID);
			response.received_request = await FriendsHelper.SentRequest(friendID, h.getUserId());
		}
		else
			response.following = await FriendsHelper.IsFollowing(h.getUserId(), friendID);

	
		h.send(response);
	}

	/**
	 * Send a friendship request
	 * 
	 * @param h Request handler
	 */
	public static async SendRequest(h: RequestHandler) {
		const friendID = await h.postUserId("friendID");

		if(friendID == h.getUserId())
			h.error(401, "You can not become a friend of yourself!");
		
		if(await FriendsHelper.AreFriend(h.getUserId(), friendID))
			h.error(401, "You are already friend with this personn!");
		
		// Check if a request is already in progress
		if(await FriendsHelper.SentRequest(h.getUserId(), friendID) 
			|| await FriendsHelper.SentRequest(friendID, h.getUserId()))
			h.error(401, "A friendship request is already in progress!");
		
		// Send the request
		await FriendsHelper.SendRequest(h.getUserId(), friendID);

		// Create the notification
		await NotificationsHelper.CreateFriendsNotifications(
			h.getUserId(), friendID, NotifEventType.SENT_FRIEND_REQUEST)

		h.success("The friendship request was successfully sent!");
	}

	/**
	 * Cancel (remove) a friendship request
	 * 
	 * @param h Request handler
	 */
	public static async CancelRequest(h: RequestHandler) {
		const friendID = await h.postUserId("friendID");

		if(!await FriendsHelper.SentRequest(h.getUserId(), friendID))
			h.error(401, "No friendship request was sent to this user!");
		
		await FriendsHelper.RemoveRequest(h.getUserId(), friendID);

		// Delete any related notification
		await NotificationsHelper.DeleteNotificationsFrienshipRequest(h.getUserId(), friendID);

		h.success("Friendship request removed!");
	}

	/**
	 * Respond to a friendship request
	 * 
	 * @param h Request handler
	 */
	public static async RespondRequest(h: RequestHandler) {
		const friendID = await h.postUserId("friendID");
		const accept = h.postBool("accept");

		if(!await FriendsHelper.SentRequest(friendID, h.getUserId()))
			h.error(401, "No friendship request was sent by this user!");
		
		await FriendsHelper.RespondRequest(h.getUserId(), friendID, accept);

		// Create notification
		await NotificationsHelper.CreateFriendsNotifications(h.getUserId(), friendID, 
			accept ? NotifEventType.ACCEPTED_FRIEND_REQUEST : NotifEventType.REJECTED_FRIEND_REQUEST);
		
		h.success("Response to the friendship request successfully saved!");
	}

	/**
	 * Remove a friend from the list
	 * 
	 * @param h Request handler
	 */
	public static async RemoveFriend(h: RequestHandler) {
		const friendID = await h.postFriendId("friendID");

		await FriendsHelper.RemoveFriendship(h.getUserId(), friendID);

		// Delete any related notification
		await NotificationsHelper.DeleteNotificationsFrienshipRequest(
			h.getUserId(), friendID);

		h.success("The friend was removed from the list!");
	}

	/**
	 * Update following status of a friendship
	 * 
	 * @param h Request handler
	 */
	public static async SetFollowing(h: RequestHandler) {
		const friendID = await h.postFriendId("friendID");
		const follow = h.postBool("follow");

		await FriendsHelper.SetFollowing(h.getUserId(), friendID, follow);

		h.success("Following status upated!");
	}

	/**
	 * Update post text authorization status
	 * 
	 * @param h Request handler
	 */
	public static async SetCanPostTexts(h: RequestHandler) {
		const friendID = await h.postFriendId("friendID");
		const allow = h.postBool("allow");

		await FriendsHelper.SetCanPostTexts(h.getUserId(), friendID, allow);

		h.success("Updated status!");
	}

	/**
	 * Turn a friend object into an API entry
	 * 
	 * @param friend Friend object to transform
	 */
	public static FriendToAPI(friend: Friend) : any {
		return {
			ID_friend: friend.friendID,
			accepted: friend.accepted ? 1 : 0,
			time_last_activity: 
				UserWebSocketController.IsConnected(friend.friendID) &&
				!UserWebSocketController.IsIcognito(friend.friendID)
				 ? time() : friend.lastActivityTime,
			following: friend.following ? 1 : 0,
			canPostTexts: friend.canPostTexts,
		}
	}

}