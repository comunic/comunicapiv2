import { RequestHandler } from "../entities/RequestHandler";
import { WebappHelper } from "../helpers/WebappHelper";
import { UserMembershipType } from "../entities/UserMembershipEntry";
import { FriendsController } from "./FriendsController";
import { Friend } from "../entities/Friend";
import { ConversationsController } from "./ConversationsController";
import { Conversation } from "../entities/Conversation";


/**
 * Web application controllers
 * 
 * @author Pierre HUBERT
 */
export class WebAppControllers {

	/**
	 * Get the memberships of the user
	 * 
	 * @param h Request handler
	 */
	public static async GetMemberships(h: RequestHandler) {
		
		const list = await WebappHelper.GetUserMemberships(h.getUserId());
		
		h.send(list.map((l) => {
			switch(l.type) {

				case UserMembershipType.FRIEND:
					return  {
						type: "friend",
						friend: FriendsController.FriendToAPI(<Friend>l.el)
					};
				
				
				case UserMembershipType.GROUP:
					return {
						type: "group",
						id: l.el,
						last_activity: l.lastActivity
					}
				
				
				case UserMembershipType.CONVERSATION:
					return {
						type: "conversation",
						conv: ConversationsController.ConversationToAPI(<Conversation>l.el)
					}

				default:
					h.error(500, "Unsupported membership type detected!");
			}
		}));
	}

}