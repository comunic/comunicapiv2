/**
 * API limits manager
 * 
 * @author Pierre HUBERT
 */

import { RequestHandler } from "../entities/RequestHandler";
import { Action, APILimitHelper } from "../helpers/APILimitsHelper";

/**
 * Trigger query limiter
 * 
 * @param h Request handler
 * @param action The action to check
 * @param trigger TRUE if the counter has to be increased by one / else it is a simple check
 */
export async function limit_query(h: RequestHandler, action: Action, trigger: boolean) {

	// Increment the number of actions / failures done by the user
	if(trigger) {
		await APILimitHelper.Trigger(h.remoteIP, action)
	}

	// Check for counter
	if(await APILimitHelper.Count(h.remoteIP, action) > 10)
		h.error(429, "Too many request. Please try again later.")
}