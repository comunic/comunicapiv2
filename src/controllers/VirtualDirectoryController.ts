import { RequestHandler } from "../entities/RequestHandler";
import { UserHelper } from "../helpers/UserHelper";
import { GroupsHelper } from "../helpers/GroupsHelper";

/**
 * Virtual directory controller
 * 
 * @author Pierre HUBERT
 */

export class VirtualDirectoryController {

	/**
	 * Find a user by its virtual directory
	 * 
	 * @param h Request handler
	 */
	public static async FindUser(h: RequestHandler) {
		const virtualDirectory = h.postVirtualDirectory("subfolder");

		const userID = await UserHelper.FindByFolder(virtualDirectory);

		if(userID < 1)
			h.error(404, "No user was found with this subfolder!");
		
		h.send({
			userID: userID
		});
	}

	/**
	 * Find a group / user using a given virtual directory
	 * 
	 * @param h Request handler
	 */
	public static async Find(h: RequestHandler) {
		const virtualDirectory = h.postVirtualDirectory("directory");

		const userID = await UserHelper.FindByFolder(virtualDirectory);
		const groupID = await GroupsHelper.FindByVirtualDirectory(virtualDirectory);

		if(userID < 1 && groupID < 1)
			h.error(404, "Specified user / group virtual directory not found !")
		
		h.send({
			kind: userID >= 1 ? "user" : "group",
			id: userID >= 1 ? userID : groupID,
		})
	}

}