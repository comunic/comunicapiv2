/**
 * User Websocket requests route
 * 
 * Note : this implementation requires the
 * user to be signed in to perform requests
 * 
 * @author Pierre Hubert
 */

import { UserWebSocketRequestsHandler } from "../entities/WebSocketRequestHandler";
import { LikesController } from "./LikesController";
import { UserWebSocketActions } from "./UserWebSocketActions";
import { CallsController } from "./CallsController";

export interface UserWebSocketRoute {
	title: string,
	handler: (h: UserWebSocketRequestsHandler) => Promise<void>
}

export const UserWebSocketRoutes: UserWebSocketRoute[] = [

	// Main controller
	{title: "$main/set_incognito", handler: (h) => UserWebSocketActions.SetIncognito(h)},

	{title: "$main/register_conv", handler: (h) => UserWebSocketActions.RegisterConv(h)},
	{title: "$main/unregister_conv", handler: (h) => UserWebSocketActions.UnregisterConv(h)},
	
	{title: "$main/register_post", handler: (h) => UserWebSocketActions.RegisterPost(h)},
	{title: "$main/unregister_post", handler: (h) => UserWebSocketActions.UnregisterPost(h)},

	// Likes controller
	{title: "likes/update", handler: (h) => LikesController.Update(h)},

	
	// Calls controller
	{title: "calls/config", handler: (h) => CallsController.GetConfig(h)},

	{title: "calls/join", handler: (h) => CallsController.JoinCall(h)},

	{title: "calls/leave", handler: (h) => CallsController.LeaveCall(h)},

	{title: "calls/members", handler: (h) => CallsController.GetMembersList(h)},

	{title: "calls/signal", handler: (h) => CallsController.OnClientSignal(h)},

	{title: "calls/mark_ready", handler: (h) => CallsController.MarkUserReady(h)},

	{title: "calls/request_offer", handler: (h) => CallsController.RequestOffer(h)},

	{title: "calls/stop_streaming", handler: (h) => CallsController.UserInterruptedStreaming(h)},
]