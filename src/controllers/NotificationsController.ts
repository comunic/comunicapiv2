import { RequestHandler } from "../entities/RequestHandler";
import { NotificationsHelper } from "../helpers/NotificationsHelper";
import { ConversationsHelper } from "../helpers/ConversationsHelper";
import { FriendsHelper } from "../helpers/FriendsHelper";
import { Notif } from "../entities/Notification";

/**
 * Notifications controller
 * 
 * @author Pierre HUBERT
 */

export class NotificationsController {

	/**
	 * Get the number of unread notifications
	 * 
	 * @param h Request handler
	 */
	public static async CountUnread(h: RequestHandler) {
		h.send({
			number: await NotificationsHelper.CountUnread(h.getUserId())
		});
	}

	/**
	 * Count the entire number of unread notifications
	 * 
	 * @param h Request handler
	 */
	public static async CountAllNews(h: RequestHandler) {
		let data = {
			notifications: await NotificationsHelper.CountUnread(h.getUserId()),
			conversations: await ConversationsHelper.CountUnreadForUser(h.getUserId())
		}

		if(h.postBool("friends_request", false))
			data["friends_requests"] = await FriendsHelper.CountRequests(h.getUserId());

		h.send(data);
	}

	/**
	 * Get the list of unread notifications
	 * 
	 * @param h Request handler
	 */
	public static async GetListUnread(h: RequestHandler) {
		const list = await NotificationsHelper.GetListUnread(h.getUserId());

		h.send(list.map(this.NotifToAPI));
	}

	/**
	 * Mark a notification as seen
	 * 
	 * @param h Request handler
	 */
	public static async MarkSeen(h: RequestHandler) {
		const notifID = await this.PostNotifyID(h, "notifID")
		const deleteSimilar = h.postBool("delete_similar", false)
		
		// Check if we are targetting a precise notification or
		// an unfinite number of similar notifications
		if(!deleteSimilar) {
			await NotificationsHelper.Delete(new Notif({
				id: notifID
			}))
		}

		else {
			// Get information about the notification
			const notif = await NotificationsHelper.GetSingle(notifID);

			await NotificationsHelper.Delete(new Notif({
				onElemType: notif.onElemType,
				onElemID: notif.onElemID,
				destUserID: h.getUserId()
			}));
		}


		h.success()
	}

	/**
	 * Delete all the notifications of a specific user
	 * 
	 * @param h Request handler
	 */
	public static async DeleteAll(h: RequestHandler) {

		await NotificationsHelper.DeleteAllUser(h.getUserId());

		h.success();
	}

	/**
	 * Get the ID of a notification included in a request
	 * 
	 * @param h Request handler
	 * @param name The name of the post field
	 */
	private static async PostNotifyID(h: RequestHandler, name: string) {
		const notifID = h.postInt(name);

		// Check if the notification exists and targets the current user
		const n = new Notif({
			id: notifID,
			destUserID: h.getUserId()
		});

		if(!await NotificationsHelper.SimilarExists(n))
			h.error(404, "Specified notification not found!");
		
		return notifID;
	}

	/**
	 * Transform a notification into an API entry
	 * 
	 * @param n The notification to transform
	 */
	private static NotifToAPI(n: Notif) : Object {
		return {
			id: n.id,
			time_create: n.timeCreate,
			seen: n.seen,
			from_user_id: n.fromUserID,
			dest_user_id: n.destUserID,
			on_elem_id: n.onElemID,
			on_elem_type: n.onElemType,
			type: n.type,
			event_visibility: n.eventVisibility,
			from_container_id: n.fromContainerID,
			from_container_type: n.fromContainerType
		};
	}
}