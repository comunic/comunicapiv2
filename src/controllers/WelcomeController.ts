import { RequestHandler } from "../entities/RequestHandler";

/**
 * Welcome controller
 * 
 * @author Pierre HUBERT
 */

export class WelcomeController {

	/**
	 * Display home message
	 */
	public static HomeMessage(req: RequestHandler) {
		req.send({
			name: "Comunic API v2",
		})
	}

}