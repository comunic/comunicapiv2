/**
 * User websocket actions
 * 
 * @author Pierre HUBERT
 */

import { UserWebSocketRequestsHandler } from "../entities/WebSocketRequestHandler";
import { UserWebSocketController } from "./UserWebSocketController";
import { WsMessage } from "../entities/WsMessage";
import { NotificationsHelper } from "../helpers/NotificationsHelper";
import { ConversationsHelper } from "../helpers/ConversationsHelper";
import { EventsHelper } from "../helpers/EventsHelper";
import { ConversationMessage } from "../entities/ConversationMessage";
import { ConversationsController } from "./ConversationsController";
import { PostAccessLevel } from "../entities/Post";
import { Comment } from "../entities/Comment";
import { CommentsController } from "./CommentsController";
import { AbritraryUserConnection } from "../entities/UserConnectionContainer";
import { CommentsHelper } from "../helpers/CommentsHelper";

export class UserWebSocketActions {

	/**
	 * Update incognito status of a user
	 * 
	 * @param h Request handler
	 */
	public static async SetIncognito(h: UserWebSocketRequestsHandler) {
		h.wsClient.incognito = h.postBool("enable", false);
		h.success();
	}

	/**
	 * Register to a conversation
	 * 
	 * @param h Request handler
	 */
	public static async RegisterConv(h: UserWebSocketRequestsHandler) {
		const convID = await h.postConversationId("convID");
		h.wsClient.registeredConversations.add(convID);
		h.success();
	}

	/**
	 * Unregister a conversation
	 * 
	 * @param h Request handler
	 */
	public static async UnregisterConv(h: UserWebSocketRequestsHandler) {
		const convID = await h.postConversationId("convID");
		h.wsClient.registeredConversations.delete(convID);
		h.success();
	}

	/**
	 * Request to be notified of post changes
	 * 
	 * @param h Request handler
	 */
	public static async RegisterPost(h: UserWebSocketRequestsHandler) {
		const postID = await h.postPostIDWithAccess("postID", PostAccessLevel.BASIC_ACCESS);
		h.wsClient.registeredPosts.add(postID);
		h.success();
	}

	/**
	 * Unregister a post
	 * 
	 * @param h Request handler
	 */
	public static async UnregisterPost(h: UserWebSocketRequestsHandler) {
		const postID = h.postInt("postID"); // Warning ! we do not check post access level here because it is not required!
		h.wsClient.registeredPosts.delete(postID);
		h.success();
	}


	/**
	 * Send updated notifications number to some users
	 * 
	 * @param usersID Target users ID
	 */
	public static async SendNewNotificationsNumber(usersID: number[]) {

		// Process each user
		for(const userID of usersID) {
			if(!UserWebSocketController.IsConnected(userID))
				continue;

			// Notify user
			UserWebSocketController.Send(userID, new WsMessage({
				title: "number_notifs",
				id: "",
				data: await NotificationsHelper.CountUnread(userID)
			}));
		}

	}

	/**
	 * Send upated number of unread conversations count
	 * 
	 * @param usersID Target users ID
	 */
	public static async SendNewUnreadConversationsCount(usersID: number[]) {
		for(const userID of usersID) {

			if(!UserWebSocketController.IsConnected(userID))
				continue;
			
			// Notify user
			UserWebSocketController.Send(userID, new WsMessage({
				title: "number_unread_conversations",
				id: "",
				data: await ConversationsHelper.CountUnreadForUser(userID)
			}));
		}
	}

	/**
	 * Propagate a new conversation message
	 * 
	 * @param msg New message
	 */
	public static async SentNewConversationMessage(msg: ConversationMessage) {
		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredConversations.has(msg.convID),
			() => WsMessage.NoIDMessage("new_conv_message", ConversationsController.ConversationMessageToAPI(msg)),
			async (c) => await ConversationsHelper.MarkUserSeen(msg.convID, c.userID)
		)
	}

	/**
	 * Propagate the update of a conversation message
	 * 
	 * @param msgID Message ID
	 */
	public static async UpdatedConversationMessage(msgID: number) {
		const msg = await ConversationsHelper.GetSingleMessage(msgID);

		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredConversations.has(msg.convID),
			() => WsMessage.NoIDMessage("updated_conv_message", ConversationsController.ConversationMessageToAPI(msg))
		)
	}

	/**
	 * Propagate the deletion of a conversation message
	 * 
	 * @param msg Message ID
	 */
	public static async DeletedConversationMessage(msg: ConversationMessage) {
		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredConversations.has(msg.convID),
			() => WsMessage.NoIDMessage("deleted_conv_message", ConversationsController.ConversationMessageToAPI(msg))
		)
	}


	/**
	 * Propagate the creation of a new comment
	 * 
	 * @param c New comment
	 */
	public static async CreatedNewComment(c: Comment) {	
		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredPosts.has(c.postID),
			async (client) => WsMessage.NoIDMessage("new_comment",
				await CommentsController.CommentToAPI(new AbritraryUserConnection(client.userID), c, true))
		)
	}

	/**
	 * Propagate comment update
	 * 
	 * @param c Updated comment
	 */
	public static async UpdatedComment(commentID: number) {
		const comment = await CommentsHelper.GetSingle(commentID);

		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredPosts.has(comment.postID),
			async (client) => WsMessage.NoIDMessage("comment_updated",
				await CommentsController.CommentToAPI(new AbritraryUserConnection(client.userID), comment))
		)
	}

	/**
	 * Propagage the deletion of a comment
	 * 
	 * @param comment Deleted comment
	 */
	public static async DeletedComment(comment: Comment) {
		await UserWebSocketController.SendToSpecifcClients(
			(e) => e.registeredPosts.has(comment.postID),
			async () => WsMessage.NoIDMessage("comment_deleted", comment.id)
		)
	}
}


// When we get a new number of notifications
EventsHelper.Listen("updated_number_notifications", async (e) => await UserWebSocketActions.SendNewNotificationsNumber(e.usersID));

// When we get a new number of unread conversations
EventsHelper.Listen("updated_number_unread_conversations", async (e) => await UserWebSocketActions.SendNewUnreadConversationsCount(e.usersID));

// When a new message is sent / updated / deleted
EventsHelper.Listen("sent_conversation_message", async (e) => await UserWebSocketActions.SentNewConversationMessage(e.msg));
EventsHelper.Listen("conv_message_updated", async (e) => await UserWebSocketActions.UpdatedConversationMessage(e.msgId));
EventsHelper.Listen("conv_message_deleted", async (e) => await UserWebSocketActions.DeletedConversationMessage(e.message));

// When a comment is created / updated / deleted
EventsHelper.Listen("comment_created", async (e) => await UserWebSocketActions.CreatedNewComment(e.comment))
EventsHelper.Listen("comment_updated", async (e) => await UserWebSocketActions.UpdatedComment(e.commentID));
EventsHelper.Listen("comment_deleted", async (e) => await UserWebSocketActions.DeletedComment(e.comment));