import { WelcomeController } from "./WelcomeController";
import { RequestHandler } from "../entities/RequestHandler";
import { AccountController } from "./AccountController";
import { UserController } from "./UserController";
import { ConversationsController } from "./ConversationsController";
import { SearchController } from "./SearchController";
import { GroupsController } from "./GroupsController";
import { NotificationsController } from "./NotificationsController";
import { VirtualDirectoryController } from "./VirtualDirectoryController";
import { WebAppControllers } from "./WebAppController";
import { CallsController } from "./CallsController";
import { FriendsController } from "./FriendsController";
import { MoviesController } from "./MoviesController";
import { PostsController } from "./PostsController";
import { CommentsController } from "./CommentsController";
import { LikesController } from "./LikesController";
import { SurveyController } from "./SurveyController";
import { SettingsController } from "./SettingsController";
import { Request } from "express";
import * as ws from 'ws';
import { UserWebSocketController } from "./UserWebSocketController";
import { RTCRelayController } from "./RTCRelayController";

/**
 * Controllers routes
 * 
 * @author Pierre Hubert
 */

export enum RouteType {
	POST, // Default
	GET,
	WS, // Special: WebSockets
}

export interface Route {
	type ?: RouteType,
	path: string,
	cb: (req : RequestHandler) => Promise<void> | void,
	needLogin ?: boolean, // Default = true

	// Specific for websockets
	wsCallback ?: (req: Request, ws: ws) => Promise<void> | void
}

export const Routes : Route[] = [

	// Main user websocket
	{path: "/ws/token", cb: (h) => UserWebSocketController.GetToken(h)},

	{type: RouteType.WS, path: "/ws", cb: () => {throw Error()}, wsCallback: (r, w) => UserWebSocketController.UserWS(r, w) },


	// RTC relay control websocket
	{type: RouteType.WS, path: "/rtc_proxy/ws", cb: () => {throw Error()}, wsCallback: (r, w) => RTCRelayController.OpenWS(r, w)},


	// Welcome controller
	{type: RouteType.GET, path: "/", cb: WelcomeController.HomeMessage, needLogin: false},

	// Account controller
	{path: "/account/create", cb: (h) => AccountController.Create(h), needLogin: false},

	{path: "/account/login", cb: (h) => AccountController.LoginUser(h), needLogin: false},
	{path: "/user/connectUSER", cb: (h) => AccountController.LoginUser(h), needLogin: false}, // Legacy

	{path: "/account/logout", cb: (h) => AccountController.LogoutUser(h)},
	{path: "/user/disconnectUSER", cb: (h) => AccountController.LogoutUser(h)}, // Legacy

	{path: "/account/disconnect_all_devices", cb: (h) => AccountController.DisconnectAllDevices(h)},
	
	{path: "/account/id", cb: (h) => AccountController.CurrentUserID(h)},
	{path: "/user/getCurrentUserID", cb: (h) => AccountController.CurrentUserID(h)}, // Legacy

	{path: "/account/exists_email", cb: (h) => AccountController.ExistsMail(h), needLogin: false},

	{path: "/account/has_security_questions", cb: (h) => AccountController.HasSecurityQuestions(h), needLogin: false},

	{path: "/account/get_security_questions", cb: (h) => AccountController.GetSecurityQuestions(h), needLogin: false},

	{path: "/account/check_security_answers", cb: (h) => AccountController.CheckSecurityAnswers(h), needLogin: false},

	{path: "/account/check_password_reset_token", cb: (h) => AccountController.CheckPasswordResetToken(h), needLogin: false},

	{path: "/account/reset_user_passwd", cb: (h) => AccountController.ResetUserPassword(h), needLogin: false},

	{path: "/account/export_data", cb: (h) => AccountController.ExportData(h)},

	{path: "/account/delete", cb: (h) => AccountController.DeleteAccount(h)},


	// User controller
	{path: "/user/getInfo", cb: (h) => UserController.GetSingle(h), needLogin: false},
	{path: "/user/getInfos", cb: (h) => UserController.GetSingle(h), needLogin: false}, // Legacy

	{path: "/user/getInfoMultiple", cb: (h) => UserController.GetMultiple(h), needLogin: false},
	{path: "/user/getInfosMultiple", cb: (h) => UserController.GetMultiple(h), needLogin: false}, // Legacy

	{path: "/user/getAdvancedUserInfo", cb: (h) => UserController.GetAdvancedInfo(h), needLogin: false},
	{path: "/user/getAdvancedUserInfos", cb: (h) => UserController.GetAdvancedInfo(h), needLogin: false}, // Legacy


	// Settings controller
	{path: "/settings/get_general", cb: (h) => SettingsController.GetGeneral(h)},

	{path: "/settings/set_general", cb: (h) => SettingsController.SetGeneral(h)},

	{path: "/settings/check_user_directory_availability", cb: (h) => SettingsController.CheckDirectoryAvailability(h)},

	{path: "/settings/get_language", cb: (h) => SettingsController.GetLanguage(h)},

	{path: "/settings/set_language", cb: (h) => SettingsController.SetLanguage(h)},

	{path: "/settings/get_security", cb: (h) => SettingsController.GetSecurity(h)},

	{path: "/settings/set_security", cb: (h) => SettingsController.SetSecurity(h)},

	{path: "/settings/check_password", cb: (h) => SettingsController.CheckUserPassword(h)},

	{path: "/settings/update_password", cb: (h) => SettingsController.UpdatePassword(h)},

	{path: "/settings/get_account_image", cb: (h) => SettingsController.GetAccountImageSettings(h)},

	{path: "/settings/upload_account_image", cb: (h) => SettingsController.UploadAccountImage(h)},

	{path: "/settings/delete_account_image", cb: (h) => SettingsController.DeleteAccountImage(h)},

	{path: "/settings/set_account_image_visibility", cb: (h) => SettingsController.SetAccountImageVisibility(h)},

	{path: "/settings/upload_custom_emoji", cb: (h) => SettingsController.UploadCustomEmoji(h)},

	{path: "/settings/delete_custom_emoji", cb: (h) => SettingsController.DeleteCustomEmoji(h)},


	// Friends controller
	{path: "/friends/getList", cb: (h) => FriendsController.GetList(h)},

	{path: "/friends/get_user_list", cb: (h) => FriendsController.GetOtherUserList(h), needLogin: false},

	{path: "/friends/get_single_infos", cb: (h) => FriendsController.GetSingleFrienshipInfo(h)},

	{path: "/friends/getStatus", cb: (h) => FriendsController.GetStatus(h)},

	{path: "/friends/sendRequest", cb: (h) => FriendsController.SendRequest(h)},

	{path: "/friends/removeRequest", cb: (h) => FriendsController.CancelRequest(h)},

	{path: "/friends/respondRequest", cb: (h) => FriendsController.RespondRequest(h)},

	{path: "/friends/remove", cb: (h) => FriendsController.RemoveFriend(h)},

	{path: "/friends/setFollowing", cb: (h) => FriendsController.SetFollowing(h)},

	{path: "/friends/set_can_post_texts", cb: (h) => FriendsController.SetCanPostTexts(h)},


	// Conversations controller
	{path: "/conversations/create", cb: (h) => ConversationsController.CreateConversation(h)},

	{path: "/conversations/getList", cb: (h) => ConversationsController.GetList(h)},

	{path: "/conversations/getInfoOne", cb: (h) => ConversationsController.GetInfoSingle(h)},
	{path: "/conversations/getInfosOne", cb: (h) => ConversationsController.GetInfoSingle(h)}, // Legacy

	{path: "/conversations/updateSettings", cb: (h) => ConversationsController.UpdateSettings(h)},

	{path: "/conversations/getPrivate", cb: (h) => ConversationsController.FindPrivate(h)},

	{path: "/conversations/refresh", cb: (h) => ConversationsController.RefreshList(h)},

	{path: "/conversations/refresh_single", cb: (h) => ConversationsController.RefreshSingleConversation(h)},

	{path: "/conversations/sendMessage", cb: (h) => ConversationsController.SendMessage(h)},

	{path: "/conversations/get_older_messages", cb: (h) => ConversationsController.GetOlderMessages(h)},

	{path: "/conversations/get_number_unread", cb: (h) => ConversationsController.CountUnreadForUser(h)},

	{path: "/conversations/get_list_unread", cb: (h) => ConversationsController.GetListUnread(h)},

	{path: "/conversations/delete", cb: (h) => ConversationsController.DeleteConversation(h)},

	{path: "/conversations/updateMessage", cb: (h) => ConversationsController.UpdateMessage(h)},

	{path: "/conversations/deleteMessage", cb: (h) => ConversationsController.DeleteMessage(h)},
	

	// Search controller
	{path: "/search/user", cb: (h) => SearchController.SearchUser(h)},
	{path: "/user/search", cb: (h) => SearchController.SearchUser(h)}, // Legacy

	{path: "/search/global", cb: (h) => SearchController.SearchGlobal(h)},


	// Groups controller
	{path: "/groups/create", cb: (h) => GroupsController.Create(h)},

	{path: "/groups/get_my_list", cb: (h) => GroupsController.GetListUser(h)},

	{path: "/groups/get_info", cb: (h) => GroupsController.GetInfoSingle(h)},

	{path: "/groups/get_multiple_info", cb: (h) => GroupsController.GetInfoMultiple(h)},

	{path: "/groups/get_advanced_info", cb: (h) => GroupsController.GetAdvancedInfo(h), needLogin: false},

	{path: "/groups/get_settings", cb: (h) => GroupsController.GetSettings(h)},

	{path: "/groups/set_settings", cb: (h) => GroupsController.SetSettings(h)},

	{path: "/groups/checkVirtualDirectory", cb: (h) => GroupsController.CheckVirtualDirectory(h)},

	{path: "/groups/upload_logo", cb: (h) => GroupsController.UploadLogo(h)},

	{path: "/groups/delete_logo", cb: (h) => GroupsController.DeleteLogo(h)},

	{path: "/groups/get_members", cb: (h) => GroupsController.GetMembers(h)},

	{path: "/groups/invite", cb: (h) => GroupsController.InviteUser(h)},

	{path: "/groups/respond_invitation", cb: (h) => GroupsController.RespondInvitation(h)},

	{path: "/groups/send_request", cb: (h) => GroupsController.SendRequest(h)},

	{path: "/groups/cancel_request", cb: (h) => GroupsController.CancelRequest(h)},

	{path: "/groups/delete_member", cb: (h) => GroupsController.DeleteMember(h)},

	{path: "/groups/update_membership_level", cb: (h) => GroupsController.UpdateMembership(h)},

	{path: "/groups/respond_request", cb: (h) => GroupsController.RespondRequest(h)},

	{path: "/groups/get_membership", cb: (h) => GroupsController.GetMembership(h)},

	{path: "/groups/cancel_invitation", cb: (h) => GroupsController.CancelInvitation(h)},

	{path: "/groups/remove_membership", cb: (h) => GroupsController.RemoveMembership(h)},

	{path: "/groups/set_following", cb: (h) => GroupsController.SetFollowing(h)},

	{path: "/groups/delete", cb: (h) => GroupsController.DeleteGroup(h)},



	// Posts controller
	{path: "/posts/get_user", cb: (h) => PostsController.GetListUser(h), needLogin: false},

	{path: "/posts/get_group", cb: (h) => PostsController.GetListGroup(h), needLogin: false},

	{path: "/posts/get_latest", cb: (h) => PostsController.GetLatest(h)},

	{path: "/posts/get_single", cb: (h) => PostsController.GetSingle(h), needLogin: false},

	{path: "/posts/create", cb: (h) => PostsController.CreatePost(h)},

	{path: "/posts/set_visibility_level", cb: (h) => PostsController.SetVisibilityLevel(h)},

	{path: "/posts/update_content", cb: (h) => PostsController.UpdateContent(h)},

	{path: "/posts/delete", cb: (h) => PostsController.DeletePost(h)},

	{path: "/posts/getAvailableTargets", cb: (h) => PostsController.GetTargets(h)},


	// Comments controller
	{path: "/comments/create", cb: (h) => CommentsController.Create(h)},

	{path: "/comments/get_single", cb: (h) => CommentsController.GetSingle(h)},

	{path: "/comments/edit", cb: (h) => CommentsController.Edit(h)},

	{path: "/comments/delete", cb: (h) => CommentsController.Delete(h)},


	// Likes controller
	{path: "/likes/update", cb: (h) => LikesController.Update(h)},


	// Surveys controller
	{path: "/surveys/get_info", cb: (h) => SurveyController.GetInfoSingle(h)},

	{path: "/surveys/send_response", cb: (h) => SurveyController.SendResponse(h)},

	{path: "/surveys/cancel_response", cb: (h) => SurveyController.CancelResponse(h)},

	{path: "/surveys/create_new_choice", cb: (h) => SurveyController.CreateNewChoice(h)},

	{path: "/surveys/block_new_choices_creation", cb: (h) => SurveyController.BlockNewChoicesCreation(h)},


	// Notifications controller
	{path: "/notifications/count_unread", cb: (h) => NotificationsController.CountUnread(h)},

	{path: "/notifications/count_all_news", cb: (h) => NotificationsController.CountAllNews(h)},

	{path: "/notifications/get_list_unread", cb: (h) => NotificationsController.GetListUnread(h)},

	{path: "/notifications/mark_seen", cb: (h) => NotificationsController.MarkSeen(h)},

	{path: "/notifications/delete_all", cb: (h) => NotificationsController.DeleteAll(h)},



	// Movies controller
	{path: "/movies/get_list", cb: (h) => MoviesController.GetList(h)},

	{path: "/movies/delete", cb: (h) => MoviesController.Delete(h)},



	// Virtual directory controller
	{path: "/user/findbyfolder", cb: (h) => VirtualDirectoryController.FindUser(h)},

	{path: "/virtualDirectory/find", cb: (h) => VirtualDirectoryController.Find(h)},


	
	// Web app controller
	{path: "/webApp/getMemberships", cb: (h) => WebAppControllers.GetMemberships(h)},

	// Calls controller
	{path: "/calls/config", cb: (h) => CallsController.GetLegacyConfig(h)},
	
]