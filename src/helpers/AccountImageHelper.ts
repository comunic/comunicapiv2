import { existsSync, unlinkSync } from "fs";
import { AccountImageVisibilityLevel } from "../entities/User";
import { DatabaseHelper } from "./DatabaseHelper";
import { USER_TABLE } from "./AccountHelper";
import { UserHelper } from "./UserHelper";

/**
 * Account image helper
 * 
 * @author Pierre HUBERT
 */

export class AccountImageHelper {

	/**
	 * Set a new account image for a user
	 * 
	 * @param userID Target user ID
	 * @param path The path for the account image
	 */
	public static async Set(userID: number, path: string) {

		// First, delete any previous account image
		await this.Delete(userID);
		
		await DatabaseHelper.UpdateRows({
			table: USER_TABLE,
			where: {
				ID: userID
			},
			set: {
				account_image_path: path
			}
		});
	}

	/**
	 * Delete (remove) a user account image
	 * 
	 * @param userID Target user ID
	 */
	public static async Delete(userID: number) {
		const user = await UserHelper.GetUserInfo(userID);
		if(user.hasAccountImage) {
			if(existsSync(user.accountImageSysPath))
				unlinkSync(user.accountImageSysPath);

			// Update database
			await DatabaseHelper.UpdateRows({
				table: USER_TABLE,
				where: {
					ID: userID
				},
				set: {
					account_image_path: ""
				}
			});
		}
	}

	/**
	 * Change account image visiblity level
	 * 
	 * @param userID Target user ID
	 * @param level New level for account image
	 */
	public static async SetVisibilityLevel(userID: number, level: AccountImageVisibilityLevel) {
		await DatabaseHelper.UpdateRows({
			table: USER_TABLE,
			where: {
				ID: userID
			},
			set: {
				account_image_visibility: level
			}
		});
	}

}