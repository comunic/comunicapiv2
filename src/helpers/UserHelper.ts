import { User, UserPageStatus } from "../entities/User";
import { DatabaseHelper } from "./DatabaseHelper";
import { AccountImageHelper } from "./AccountImageHelper";
import { FriendsHelper } from "./FriendsHelper";

/**
 * User helper
 * 
 * @author Pierre HUBERT
 */

const TABLE_NAME = "utilisateurs";

export class UserHelper {

	/**
	 * Get information a single user
	 * 
	 * @param id The ID of the user to get
	 * @returns Information about the user | null if not found
	 */
	public static async GetUserInfo(id: number) : Promise<User|null> {
		const result = await DatabaseHelper.QueryRow({
			table: TABLE_NAME,
			where: {
				ID: id
			}
		});

		if(!result)
			return null;
		
		return this.DbToUser(result);
	}

	/**
	 * Check out whether a user exists or not
	 * 
	 * @param id The ID of the user to check
	 */
	public static async Exists(id: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: TABLE_NAME,
			where: {
				ID: id
			}
		}) > 0;
	}

	/**
	 * Search for user in the database
	 * 
	 * @param query The query
	 * @param limit Limit for the request (default: 10)
	 * @returns The list of ids of the user
	 */
	public static async SearchUser(query: string, limit: number = 10) : Promise<Array<number>> {
		
		query = "%" + query.replace(/\ /g, "%") + "%";
		
		const request = await DatabaseHelper.Query({
			fields: ["ID"],
			table: TABLE_NAME,
			customWhere: "(nom LIKE ?) || (prenom LIKE ?) || (CONCAT(prenom, '%', nom) LIKE ?) || (CONCAT(nom, '%', prenom) LIKE ?)",
			customWhereArgs: [query, query, query, query],
			limit: limit,
		});

		return request.map((e) => e.ID);
	}

	/**
	 * Search for user by virtual directory
	 * 
	 * @param dir Target directory
	 * @returns The ID of the user found / -1 if none found
	 */
	public static async FindByFolder(dir: string) : Promise<number> {
		const result = await DatabaseHelper.QueryRow({
			table: TABLE_NAME,
			where: {
				sous_repertoire: dir
			},
			fields: ["ID"]
		});

		return result == null ? -1 : Number(result.ID);
	}

	/**
	 * Check out whether a user is allowed to access another
	 * user's page
	 * 
	 * @param userID The ID of the user making the request
	 * @param targetUser The target user page
	 */
	public static async CanSeeUserPage(userID: number, targetUser: number) : Promise<boolean> {
		
		if(userID == targetUser)
			return true;
		
		const visibility = await this.GetVisibility(targetUser);
		
		// Open page = OK
		if(visibility == UserPageStatus.OPEN) return true;

		// Else the user must be signed in
		if(userID <= 0) return false;

		// Public page = OK for signed in users
		if(visibility == UserPageStatus.PUBLIC) return true;

		// Check if the two users are friend
		if(!await FriendsHelper.AreFriend(userID, targetUser)) return false;

		return true;
	}

	/**
	 * Convenience method to get the visibility level of a user
	 * 
	 * @param userID ID of the target user
	 */
	private static async GetVisibility(userID: number) : Promise<UserPageStatus> {
		return (await this.GetUserInfo(userID)).pageStatus;
	}

	/**
	 * Check out whether a user allows posts from his 
	 * friends on his page or not
	 * 
	 * This is a convenience method
	 * 
	 * @param userID Target user ID
	 */
	private static async AllowPostsOnHisPage(userID: number) : Promise<boolean> {
		return (await this.GetUserInfo(userID)).allowPostsFromFriends;
	}

	/**
	 * Convenience function to check whether a friend list is
	 * public or not
	 * 
	 * @param userID Target user ID
	 */
	public static async IsUserFriendListPublic(userID: number) : Promise<boolean> {
		return (await this.GetUserInfo(userID)).friendsListPublic;
	}

	/**
	 * Convenience function to check out whether comments are
	 * allowed on a page or not
	 * 
	 * @param userID Target user ID
	 */
	public static async AllowComments(userID: number) : Promise<boolean> {
		return !(await this.GetUserInfo(userID)).blockComments;
	}

	/**
	 * Check out whether a user can create a post on another user
	 * page
	 * 
	 * @param userID The user who wish to create a post
	 * @param targetID The target page
	 */
	public static async CanCreatePosts(userID: number, targetID: number) {
		
		// User MUST be signed in
		if(userID < 1)
			return false;
		
		// A user can always create posts on his page
		if(userID == targetID) return true;

		// User must be able to see the page
		if(!await this.CanSeeUserPage(userID, targetID))
			return false;
		
		//Check if the user allow posts on his page
		if(!await this.AllowPostsOnHisPage(targetID))
			return false;
		
		//Check if the friendship of the user allows him to create posts
		if(!await FriendsHelper.CanPostTexts(userID, targetID))
			return false;

		// yes by default
		return true;
	}


	private static async DbToUser(row: any) : Promise<User> {
		return new User({
			id: row.ID,
			email: row.mail,
			firstName: row.prenom,
			lastName: row.nom,
			timeCreate: new Date(row.date_creation).getTime()/1000,
			virtualDirectory: row.sous_repertoire,
			pageStatus: row.pageouverte == 1 ? UserPageStatus.OPEN : (row.public == 1 ? UserPageStatus.PUBLIC : UserPageStatus.PRIVATE),
			accountImagePath: row.account_image_path,
			accountImageVisibilityLevel: row.account_image_visibility,
			friendsListPublic: row.liste_amis_publique == 1,
			personnalWebsite: row.site_web,
			publicNote: row.public_note,
			blockComments: row.bloquecommentaire == 1,
			allowPostsFromFriends: row.autoriser_post_amis == 1,
			allowMails: row.autorise_mail == 1,
			lang: row.lang,
			security_question_1: row.question1,
			security_answer_1: row.reponse1,
			security_question_2: row.question2,
			security_answer_2: row.reponse2
		});
	}
}