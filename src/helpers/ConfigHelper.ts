import { readFileSync } from "fs";


/**
 * Configuration helper
 * 
 * @author Pierre HUBERT
 */

export interface DatabaseConfiguration {
	host : string,
	dbName : string,
	user : string,
	password : string,
	dbPrefix : string
}

export interface RTCRelayConfiguration {
	ip ?: string,
	token: string,
	iceServers: string[],

	maxUsersPerCalls: number,
	allowVideo: boolean,
	maxUsersPerVideoCalls: number,
}

export interface Configuration {
	
	// Server listening port
	port: number,

	// Access to user data
	storageURL : string,
	storagePath : string,

	// Database connection configuration
	database : DatabaseConfiguration,

	// URL of trusted proxy (to get remote IP address)
	proxy ?: string,

	// Set access-control-allow-origin to require HTTPS
	force_clients_https ?: boolean,

	// RTC (for calls) proxy configuration
	rtc_relay ?: RTCRelayConfiguration,
}

export class ConfigurationHelper {

	private static conf : Configuration;
	
	public static getConf() : Configuration {
		return this.conf;
	}

	/**
	 * Load configuration from a specified configuration file
	 * 
	 * @param fileName Configuration file to load
	 */
	public static loadConf(fileName : string) {
		this.conf = JSON.parse(readFileSync(fileName, {encoding: "utf-8"}));
	}
}

/**
 * Get project configuration
 */
export function conf() : Configuration {
	return ConfigurationHelper.getConf();
}
