import { APIClient } from "../entities/APIClient";
import { DatabaseHelper } from "./DatabaseHelper";

/**
 * API Helper
 * 
 * @author Pierre HUBERT
 */

export class APIHelper {
	
	private static SERVICES_TABLE = "comunic_api_services_tokens";

	/**
	 * Get information about an API client
	 * 
	 * @param name The name of the client to get
	 * @param token The token of the client to get
	 * @returns Information about the client or null
	 * if none found
	 */
	public static async GetClient(name: string, token: string) : Promise<APIClient | null> {
		
		const entry = await DatabaseHelper.QueryRow({
			table: this.SERVICES_TABLE,
			where: {
				service_name: name,
				token: token
			},
			limit: 1
		});

		if(entry == null)
			return null;

		return {
			id: entry.id,
			name: entry.service_name,
			token: entry.token,
			domain: entry.client_domain
		};
	}

}