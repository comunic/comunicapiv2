import { pathUserData } from "../utils/UserDataUtils";

/**
 * Legacy background image helper
 * 
 * @author Pierre HUBERT
 */

export class BackgroundImageHelper {

	public static GetURL(userID: number) : string {
		// Currently, it is not planned to integrate background image anymore
		return pathUserData("imgfond/0.jpg");
	}

	public static async Delete(userID: number) {
		// Currently, it is not planned to integrate background image anymore
	}

}