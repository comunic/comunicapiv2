/**
 * Calls helper
 * 
 * @author Pierre Hubert
 */

import { Conversation } from "../entities/Conversation";
import { conf } from "./ConfigHelper";
import { RTCRelayController } from "../controllers/RTCRelayController";

export class CallsHelper {

	/**
	 * Check out whether a given conversation can have a call or not
	 * 
	 * @param conv Target conversation
	 */
	public static CanHaveCall(conv: Conversation) : boolean {
		return (conf().rtc_relay && RTCRelayController.IsConnected
			&& conv.members.size > 1
			&& conf().rtc_relay.maxUsersPerCalls >= conv.members.size) === true
	}

	/**
	 * Check out whether a given conversation is allowed to make video
	 * calls or not
	 * 
	 * @param conv Target conversation
	 */
	public static CanHaveVideoCAll(conv: Conversation) : boolean {
		return this.CanHaveCall(conv)
			&& conf().rtc_relay.allowVideo
			&& conf().rtc_relay.maxUsersPerVideoCalls >= conv.members.size;
	}

}