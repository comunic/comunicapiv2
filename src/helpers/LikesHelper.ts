import { DatabaseHelper } from "./DatabaseHelper";
import { mysql_date } from "../utils/DateUtils";
import { UserLike } from "../entities/UserLike";

/**
 * Likes helper
 * 
 * @author Pierre HUBERT
 */

/**
 * Types of likes
 */
export enum LikesType {
	USER = "user",
	POST = "post",
	COMMENT = "comment",
	GROUP = "group"
}


const LIKES_TABLE = "aime";


/**
 * Translation of kinds of likes for the database
 */
const LikesKindsDB = [];
LikesKindsDB[LikesType.USER] = "page";
LikesKindsDB[LikesType.POST] = "texte";
LikesKindsDB[LikesType.COMMENT] = "commentaire";
LikesKindsDB[LikesType.GROUP] = "group";



export class LikesHelper {

	/**
	 * Count the number of likes of a specified element
	 * 
	 * @param id The ID of the element to count
	 * @param type The type of the element
	 */
	public static async Count(id: number, type: LikesType) : Promise<number> {
		return DatabaseHelper.Count({
			table: LIKES_TABLE,
			where: {
				ID_type: id,
				type: LikesKindsDB[type]
			}
		})
	}


	/**
	 * Check out whether the user likes an element or not
	 * 
	 * @param userID ID of the user
	 * @param id ID of the thing to check 
	 * @param type The kind of the element
	 */
	public static async IsLiking(userID: number, id: number, type: LikesType) : Promise<boolean> {
		if(userID == 0) return false;

		return await DatabaseHelper.Count({
			table: LIKES_TABLE,
			where: {
				ID_type: id,
				type: LikesKindsDB[type],
				ID_personne: userID
			}
		}) == 1;
	}

	/**
	 * Update like status
	 * 
	 * @param userID The ID of the user updating like status
	 * @param isLiking New like status of the user
	 * @param id The ID of the component element to update
	 * @param kind The kind of the element
	 */
	public static async Update(userID: number, isLiking: boolean, id: number, kind: LikesType) {

		// If not liking anymore
		if(!isLiking) {
			await DatabaseHelper.DeleteRows(LIKES_TABLE, {
				ID_personne: userID,
				ID_type: id,
				type: LikesKindsDB[kind]
			})

			return;
		}

		// Else we make the user liking the element

		// Check if the user is already liking the element
		if(await this.IsLiking(userID, id, kind))
			return;
		
		// Insert in the database
		await DatabaseHelper.InsertRow(LIKES_TABLE, {
			ID_personne: userID,
			ID_type: id,
			Date_envoi: mysql_date(),
			type: LikesKindsDB[kind]
		});
	}

	/**
	 * Delete all the likes related to an element
	 * 
	 * @param id Target element ID
	 * @param type The type of target element
	 */
	public static async DeleteAll(id: number, type: LikesType) {
		await DatabaseHelper.DeleteRows(LIKES_TABLE, {
			ID_type: id,
			type: LikesKindsDB[type]
		});
	}

	/**
	 * Get all the likes of a user
	 * 
	 * @param userID Target user ID
	 */
	public static async ExportAllUser(userID: number) {
		return (await DatabaseHelper.Query({
			table: LIKES_TABLE,
			where: {
				ID_personne: userID
			}
		})).map(this.DBToUserLike);
	}

	/**
	 * Delete all the likes of a given user
	 * 
	 * @param userID Target user ID
	 */
	public static async DeleteAllUser(userID: number) {
		await DatabaseHelper.DeleteRows(LIKES_TABLE, {
			ID_personne: userID
		})
	}

	/**
	 * Turn a database entry into a like entry
	 * 
	 * @param row Target database row
	 */
	private static DBToUserLike(row: any) : UserLike {
		return new UserLike({
			id: row.ID,
			userID: row.ID_personne,
			timeSent: new Date(row.Date_envoi).getTime()/1000,
			elemId: row.ID_type,
			elemType: row.type
		})
	}
}