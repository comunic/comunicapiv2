import { Comment } from "../entities/Comment";
import { DatabaseHelper } from "./DatabaseHelper";
import { unlinkSync, existsSync } from "fs";
import { LikesHelper, LikesType } from "./LikesHelper";
import { mysql_date, time } from "../utils/DateUtils";
import { EventsHelper } from "./EventsHelper";

/**
 * Comments helper
 * 
 * @author Pierre HUBERT
 */

const COMMENTS_TABLE = "commentaires";

export class CommentsHelper {

	/**
	 * Create a new comment
	 * 
	 * @param comment Information about the comment to create
	 */
	public static async Create(comment: Comment) : Promise<number> {
		const commentID = await DatabaseHelper.InsertRow(COMMENTS_TABLE, {
			ID_texte: comment.postID,
			ID_personne: comment.userID,
			date_envoi: mysql_date(),
			time_insert: time(),
			commentaire: comment.content,
			image_commentaire: comment.hasImage ? comment.imagePath : ""
		});

		// Propagate event
		comment.id = commentID;
		await EventsHelper.Emit("comment_created", {
			comment: comment
		});

		return commentID
	}

	/**
	 * Edit the content of a comment
	 * 
	 * @param commentID Target comment
	 * @param newContent New content
	 */
	public static async Edit(commentID: number, newContent: string) {
		await DatabaseHelper.UpdateRows({
			table: COMMENTS_TABLE,
			where: {
				ID: commentID
			},
			set: {
				commentaire: newContent
			}
		})

		await EventsHelper.Emit("comment_updated", {
			commentID: commentID
		});
	}

	/**
	 * Get the comments of a POST
	 * 
	 * @param postID Target post ID
	 */
	public static async Get(postID: number) : Promise<Array<Comment>> {
		const results = await DatabaseHelper.Query({
			table: COMMENTS_TABLE,
			where: {
				ID_texte: postID
			},
			order: "ID"
		});

		return results.map(this.DbToComment);
	}

	/**
	 * Check out whether a comment exists or not
	 * 
	 * @param commentID Target comment ID
	 */
	public static async Exists(commentID: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: COMMENTS_TABLE,
			where: {
				ID: commentID
			}
		}) > 0;
	}

	/**
	 * Check out whether a comment belongs to a user or not
	 * 
	 * @param commentID Target comment ID
	 */
	public static async IsOwner(userID: number, commentID: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: COMMENTS_TABLE,
			where: {
				ID: commentID,
				ID_personne: userID
			}
		}) > 0;
	}

	/**
	 * Get information about a single comment
	 * 
	 * @param commentID Target comment ID
	 */
	public static async GetSingle(commentID: number) : Promise<Comment> {
		const row = await DatabaseHelper.QueryRow({
			table: COMMENTS_TABLE,
			where: {
				ID: commentID
			}
		});

		if(row == null)
			throw new Error("Comment " + commentID + " not found!");
		
		return this.DbToComment(row);
	}

	/**
	 * Get the ID of the post associated to a comment
	 * 
	 * @param commentID Target comment ID
	 */
	public static async GetAssociatedPost(commentID: number) : Promise<number> {
		const comment = await this.GetSingle(commentID);

		return comment.postID;
	}

	/**
	 * Permanently delete a comment
	 * 
	 * @param comment Information about the comment to delete
	 */
	public static async Delete(comment: Comment) {

		// Delete associated image (if any)
		if(comment.hasImage && existsSync(comment.imageSysPath)) {
			unlinkSync(comment.imageSysPath);
		}

		// Delete likes associated with the comment
		await LikesHelper.DeleteAll(comment.id, LikesType.COMMENT);

		// Delete the comment from the database
		await DatabaseHelper.DeleteRows(COMMENTS_TABLE, {
			ID: comment.id
		})

		// Notify system
		await EventsHelper.Emit("comment_deleted", {
			comment: comment
		});
	}

	/**
	 * Delete all the comments associated to a post
	 * 
	 * @param postID Target post ID
	 */
	public static async DeleteAll(postID: number) {
		
		// Get the comments of the post
		for(const comment of await this.Get(postID)) {
			await this.Delete(comment);
		}
	}

	/**
	 * Get all the comments sent by a specified user
	 * 
	 * @param userID Target user ID
	 */
	public static async ExportAllUser(userID: number) : Promise<Comment[]> {
		return (await DatabaseHelper.Query({
			table: COMMENTS_TABLE,
			where: {
				ID_personne: userID
			}
		})).map((row) => this.DbToComment(row));
	}

	/**
	 * Delete all the comment of a given user
	 * 
	 * @param userID Target user id
	 */
	public static async DeleteAllUser(userID: number) {
		for(const el of await this.ExportAllUser(userID))
			await this.Delete(el);
	}

	/**
	 * Turn a database entry into a Comment object
	 * 
	 * @param row Database entry
	 */
	private static DbToComment(row: any) : Comment {
		return new Comment({
			id: row.ID,
			userID: row.ID_personne,
			postID: row.ID_texte,
			timeSent: row.time_insert == null ? new Date(row.date_envoi).getTime() / 100 : row.time_insert,
			content: row.commentaire,
			imagePath: (row.image_commentaire != null && row.image_commentaire != "") ? row.image_commentaire.replace("file:", "") : null
		});
	}
}