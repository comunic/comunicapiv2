import { UserMembershipEntry, UserMembershipType } from "../entities/UserMembershipEntry";
import { FriendsHelper } from "./FriendsHelper";
import { GroupsHelper } from "./GroupsHelper";
import { ConversationsHelper } from "./ConversationsHelper";
import { UserWebSocketController } from "../controllers/UserWebSocketController";
import { time } from "../utils/DateUtils";

/**
 * Web applications helper
 * 
 * @author Pierre HUBERT
 */

export class WebappHelper {

	/**
	 * Get all the membership of a user
	 * 
	 * @param userID Target user ID
	 */
	public static async GetUserMemberships(userID: number) : Promise<Array<UserMembershipEntry>> {

		// Get the list of friends of the user
		const friendsList = await FriendsHelper.GetList(userID);
		const groupsList = await GroupsHelper.GetListUser(userID);
		const conversationsList = await ConversationsHelper.GetListUser(userID);

		const list : Array<UserMembershipEntry> = [];

		// Add friends to the list
		friendsList.forEach((f) => list.push(new UserMembershipEntry(
			UserWebSocketController.IsConnected(f.friendID) ? time() : f.lastActivityTime,
			f, 
			UserMembershipType.FRIEND
		)));
		
		// Add groups to the list
		for(const g of groupsList) list.push(new UserMembershipEntry(
			await GroupsHelper.GetLastActivity(userID, g), g, UserMembershipType.GROUP));

		// Add conversations to the list
		conversationsList.forEach(c => list.push(new UserMembershipEntry(
			c.lastActive,
			c,
			UserMembershipType.CONVERSATION
		)))

		// Sort entries by latest activities
		list.sort((a, b) => b.lastActivity - a.lastActivity);

		return list;
	}

}