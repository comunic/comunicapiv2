/**
 * Custom emojies helper
 * 
 * @author Pierre Hubert
 */

import { CustomEmoji } from "../entities/CustomEmoji";
import { DatabaseHelper } from "./DatabaseHelper";
import { existsSync, unlinkSync } from "fs";

const EMOJIS_TABLE = "comunic_custom_emojis";

export class CustomEmojisHelper {

	/**
	 * Insert a new custom emoji into the database
	 * 
	 * @param e The emoji to insert
	 * @return The ID of the created emoji
	 */
	public static async Insert(e: CustomEmoji) : Promise<number> {
		return await DatabaseHelper.InsertRow(
			EMOJIS_TABLE,
			{
				user_id: e.userID,
				shortcut: e.shortcut,
				path: e.path
			}
		)
	}


	/**
	 * Check if a given user has already a custom emoji with
	 * the same shortcut
	 * 
	 * @param userID Target user ID
	 * @param shortcut The name of the shortcut to check
	 */
	public static async HasUserSimilarShortcut(userID: number, shortcut: string) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: EMOJIS_TABLE,
			where: {
				user_id: userID,
				shortcut: shortcut
			}
		}) > 0;
	}

	/**
	 * Get the list of emojies of a given user
	 * 
	 * @param userID Target user ID
	 */
	public static async GetListUser(userID: number) : Promise<CustomEmoji[]> {
		return (await DatabaseHelper.Query({
			table: EMOJIS_TABLE,
			where: {
				user_id: userID
			}
		})).map((e) => this.DbToCustomEmoji(e));
	}

	/**
	 * Get information about a single emoji
	 * 
	 * @param emojiID The ID of the emoji to get
	 * @throws An error if the emoji was not found
	 */
	public static async GetSingle(emojiID: number) : Promise<CustomEmoji> {
		const row = await DatabaseHelper.QueryRow({
			table: EMOJIS_TABLE,
			where: {
				id: emojiID
			}
		});

		if(row == null)
			throw new Error("Requested emoji was not found ! (emoji id " + emojiID + ")")

		return this.DbToCustomEmoji(row);
	}

	/**
	 * Delete a custom emoji
	 * 
	 * @param e Information about the emoji to delete
	 */
	public static async Delete(e: CustomEmoji) {

		if(existsSync(e.sysPath))
			unlinkSync(e.sysPath)
		
		await DatabaseHelper.DeleteRows(EMOJIS_TABLE, {
			id: e.id
		})

	}

	/**
	 * Delete all the emojies of the user
	 * 
	 * @param userID Target user ID
	 */
	public static async DeleteAllUser(userID: number) {
		for(const emoji of await this.GetListUser(userID))
			await this.Delete(emoji);
	}

	/**
	 * Turn a database entry into a custom emoji
	 * 
	 * @param row Database entry
	 */
	private static DbToCustomEmoji(row: any) : CustomEmoji {
		return new CustomEmoji({
			id: row.id,
			userID: row.user_id,
			shortcut: row.shortcut,
			path: row.path
		})
	}
}