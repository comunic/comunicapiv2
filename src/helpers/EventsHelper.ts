import { APIClient } from "../entities/APIClient";
import { ConversationMessage } from "../entities/ConversationMessage";
import { Comment } from "../entities/Comment";
import { ActiveClient } from "../controllers/UserWebSocketController";

/**
 * Events manager
 * 
 * @author Pierre Hubert
 */

// When RTC Relay WebSocket is closed
export interface ClosedRelayWebSocketEvent {}

// When a signal is sent from the RTC proxy
export interface CallSignalFromRTCRelayEvent {
	peerID: string,
	callHash: string,
	data: any
}

// When a user sign out
export interface DestroyedLoginTokensEvent {
	userID: number,
	client: APIClient
}

// When a user WebSocket connection is closed
export interface UserWebSocketClosedEvent {
	client: ActiveClient
}

// When some user might have an updated number of notifications
export interface UpdatedNotificationsNumberEvent {
	usersID: number[]
}

// When some users have an updated number of unread conversations
export interface UpdateNumberUnreadConversationsEvent {
	usersID: number[]
}

// When a new conversatino message is sent
export interface SentNewConversationMessageEvent {
	msg: ConversationMessage
}

// When a conversation message is updated
export interface UpdatedConversationMessageEvent {
	msgId: number
}

// When a conversation message was deleted
export interface DeletedConversationMessageEvent {
	message: ConversationMessage
}

// When a comment is created
export interface CommentCreatedEvent {
	comment: Comment
}

// When a comment is updated
export interface CommentUpdatedEvent {
	commentID: number
}

// When a comment is deleted
export interface CommentDeletedEvent {
	comment: Comment
}

/**
 * Global map of all possible events
 */
export interface EventsMap {
	"rtc_relay_ws_closed": ClosedRelayWebSocketEvent,
	"rtc_relay_signal": CallSignalFromRTCRelayEvent,
	"destroyed_login_tokens": DestroyedLoginTokensEvent,
	"user_ws_closed": UserWebSocketClosedEvent,
	"updated_number_notifications": UpdatedNotificationsNumberEvent,
	"updated_number_unread_conversations": UpdateNumberUnreadConversationsEvent,
	"sent_conversation_message": SentNewConversationMessageEvent,
	"conv_message_updated": UpdatedConversationMessageEvent,
	"conv_message_deleted": DeletedConversationMessageEvent,
	"comment_created": CommentCreatedEvent,
	"comment_updated": CommentUpdatedEvent,
	"comment_deleted": CommentDeletedEvent,
}

export class EventsHelper {

	/**
	 * The list of events
	 */
	private static EventsList: Map<keyof EventsMap, Array<(ev: any) => void | Promise<void>>> = new Map()

	/**
	 * Listen to a new event
	 * 
	 * @param name The name of the event to listen to
	 * @param listener Target listener
	 */
	public static Listen<K extends keyof EventsMap>(name: K, listener: (ev: EventsMap[K]) => void | Promise<void>) {
		if(!this.EventsList[<any>name])
			this.EventsList[<any>(name)] = [];

		this.EventsList[<any>(name)].push(listener);
		
	}

	/**
	 * Push a new event
	 * 
	 * Note : It is not mandatory to listen to this method, as
	 * we will try / catch all the events
	 * 
	 * @param name The name of the event
	 * @param args Argumens to pass to the event
	 */
	public static async Emit<K extends keyof EventsMap>(name: K, args: EventsMap[K]) {
		
		if(!this.EventsList[<any>name])
			return;

		for(const entry of this.EventsList[<any>name]) {
			try {
				await entry(args);
			}
			catch(e) {
				console.error("An error occured while propagating an event ("+name+")!", e, args);
			}
		}
	}

	
}