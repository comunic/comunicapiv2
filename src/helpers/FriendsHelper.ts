import { DatabaseHelper } from "./DatabaseHelper";
import { USER_TABLE } from "./AccountHelper";
import { Friend } from "../entities/Friend";

/**
 * Friends helper
 * 
 * @author Pierre HUBERT
 */

const FRIENDS_TABLE = "amis";

export class FriendsHelper {

	/**
	 * Get the list of friends of the user
	 * 
	 * @param userID The ID of the user
	 * @param onlyAccepted Return only accepted friendship
	 */
	public static async GetList(userID: number, onlyAccepted = false) : Promise<Array<Friend>> {
		const results = await DatabaseHelper.Query({
			table: FRIENDS_TABLE + " f",
			where: {
				ID_personne: userID,
			},
			customWhere: onlyAccepted ? "actif = 1" : "",
			joins: [
				{
					table: USER_TABLE + " u",
					condition: "f.ID_amis = u.ID"
				}
			],
			fields: [
				"u.last_activity",
				"f.ID_amis",
				"f.actif",
				"f.abonnement",
				"f.autoriser_post_page"
			],
			order: "u.last_activity DESC"
		});

		return results.map((r) => this.DbToFriend(r));
	}

	/**
	 * Get the list of friends of a given user who allows
	 * him to create post on their pages
	 * 
	 * @param userID ID of the user making the request
	 */
	public static async GetListThatAllowsPostsFromUser(userID: number) : Promise<Array<number>> {
		const list = await DatabaseHelper.Query({
			table: FRIENDS_TABLE,
			where: {
				autoriser_post_page: 1,
				actif: 1,
				ID_amis: userID
			},
			fields: ["ID_personne as user_id"]
		});

		return list.map((e) => e.user_id);
	}

	/**
	 * Get information about a single membership
	 * 
	 * @param userID Target user ID (ID of the user making the request)
	 * @param friendID Target friend ID
	 * @returns Information about the membership / null if no membership was found
	 */
	public static async GetSingleInfo(userID: number, friendID: number) : Promise<Friend> {
		
		// Same request has above, but just for a single user
		const result = await DatabaseHelper.QueryRow({
			table: FRIENDS_TABLE + " f",
			where: {
				ID_personne: userID,
				ID_amis: friendID
			},
			joins: [
				{
					table: USER_TABLE + " u",
					condition: "f.ID_amis = u.ID"
				}
			],
			fields: [
				"u.last_activity",
				"f.ID_amis",
				"f.actif",
				"f.abonnement",
				"f.autoriser_post_page"
			],
			order: "u.last_activity DESC"
		});

		if(result == null)
			return null;

		return this.DbToFriend(result);
	}

	/**
	 * Send (create) a new membership request
	 * 
	 * @param userID The user sending the request
	 * @param targetUser The ID of the target user
	 */
	public static async SendRequest(userID: number, targetUser: number) {
		await DatabaseHelper.InsertRow(
			FRIENDS_TABLE,
			{
				ID_personne: targetUser,
				ID_amis: userID,
				actif: 0
			}
		);
	}

	/**
	 * Check out whether a user sent a request to another friend
	 * 
	 * @param userID The ID of the user supposed to have sent a request
	 * @param targetUser The ID of the target user
	 */
	public static async SentRequest(userID: number, targetUser: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: targetUser,
				ID_amis: userID,
				actif: 0
			}
		}) > 0;
	}

	/**
	 * Delete a previously sent friendship request
	 * 
	 * @param userID The ID of the user who have sent the request
	 * @param targetUser The ID of the target user
	 */
	public static async RemoveRequest(userID: number, targetUser: number) {
		await DatabaseHelper.DeleteRows(
			FRIENDS_TABLE,
			{
				ID_personne: targetUser,
				ID_amis: userID,
				actif: 0
			}
		);
	}
	
	/**
	 * Respond to a friendship request
	 * 
	 * @param userID The ID of the user who respond to the request
	 * @param friendID The ID of the user who sent the request
	 * @param accept TRUE to accept / FALSE else
	 */
	public static async RespondRequest(userID: number, friendID: number, accept: boolean) : Promise<boolean> {
		
		// Delete the request
		await this.RemoveRequest(friendID, userID);

		// If the request was rejected
		if(!accept)
			return;

		// If the request was accepted
		await DatabaseHelper.InsertRow(FRIENDS_TABLE, {
			ID_personne: userID,
			ID_amis: friendID,
			actif: 1
		});

		await DatabaseHelper.InsertRow(FRIENDS_TABLE, {
			ID_personne: friendID,
			ID_amis: userID,
			actif: 1
		});
	}

	/**
	 * Destroy a friendship
	 *
	 * @param userOne The ID of the first user
	 * @param userTwo The ID of the second user
	 */
	public static async RemoveFriendship(userOne: number, userTwo: number) {
		await DatabaseHelper.DeleteRows(FRIENDS_TABLE, {
			ID_personne: userOne,
			ID_amis: userTwo
		});

		await DatabaseHelper.DeleteRows(FRIENDS_TABLE, {
			ID_personne: userTwo,
			ID_amis: userOne
		});
	}

	/**
	 * Count the number of friendship requests a user
	 * received
	 * 
	 * @param userID Target user ID
	 */
	public static async CountRequests(userID: number) : Promise<number> {
		return await DatabaseHelper.Count({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: userID,
				actif: 0
			}
		});
	}

	/**
	 * Count the number of friends of a specific user
	 * 
	 * @param userID Target user ID
	 */
	public static async CountForUser(userID: number) : Promise<number> {
		return await DatabaseHelper.Count({
			table: FRIENDS_TABLE,
			where: {
				ID_amis: userID,
				actif: 1
			}
		});
	}

	/**
	 * Check out whether two users are friend or not
	 * 
	 * @param userOne First user
	 * @param userTwo Second user
	 */
	public static async AreFriend(userOne: number, userTwo: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: userOne,
				ID_amis: userTwo,
				actif: 1
			}
		}) > 0;
	}

	/**
	 * Update the following status for a friendship
	 * 
	 * @param userID The ID of the user updating the status
	 * @param friendID The ID of the target friend
	 * @param follow New following status
	 */
	public static async SetFollowing(userID: number, friendID: number, follow: boolean) {
		await DatabaseHelper.UpdateRows({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: userID,
				ID_amis: friendID
			},
			set: {
				abonnement: follow ? 1 : 0
			}
		})
	}

	/**
	 * Check whether a user is following a friend or not
	 * 
	 * @param userID The ID of the user supposed to following a friend
	 * @param friendID The target friend
	 */
	public static async IsFollowing(userID: number, friendID: number) : Promise<boolean> {
		return await DatabaseHelper.Count({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: userID,
				ID_amis: friendID,
				actif: 1,
				abonnement: 1
			}
		}) > 0;
	}

	/**
	 * Update the post creation authorization status
	 * 
	 * @param userID The ID of the user updating the authorization status
	 * @param friendID The ID of the target friend
	 * @param allow New authorization status
	 */
	public static async SetCanPostTexts(userID: number, friendID: number, allow: boolean) {
		await DatabaseHelper.UpdateRows({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: userID,
				ID_amis: friendID
			},
			set: {
				autoriser_post_page: allow ? 1 : 0
			}
		});
	}

	/**
	 * Check out whether friendship allows to create posts or not
	 * 
	 * @param userID User wishing to create a post
	 * @param targetID Target user ID
	 */
	public static async CanPostTexts(userID: number, targetID: number) : Promise<boolean> {
		const result = await DatabaseHelper.QueryRow({
			table: FRIENDS_TABLE,
			where: {
				ID_personne: targetID,
				ID_amis: userID,
				actif: 1
			},
			fields: ["autoriser_post_page"]
		});

		if(result == null)
			return false;
		
		return result["autoriser_post_page"] == 1;
	}

	/**
	 * Delete all the friends of a given user
	 * 
	 * @param userID Target user ID
	 */
	public static async DeleteAllUser(userID: number) {
		await DatabaseHelper.DeleteRows(FRIENDS_TABLE, {
			ID_personne: userID
		})

		await DatabaseHelper.DeleteRows(FRIENDS_TABLE, {
			ID_amis: userID
		})
	}

	/**
	 * Turn a database entry into a {Friend} object
	 * 
	 * @param row The row to transform
	 */
	private static DbToFriend(row: any) : Friend {
		return new Friend({
			friendID: row.ID_amis,
			accepted: row.actif == 1,
			following: row.abonnement == 1,
			lastActivityTime: row.last_activity,
			canPostTexts: row.autoriser_post_page == 1
		});
	}
}