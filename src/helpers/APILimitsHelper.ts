/**
 * API Limits helper
 * 
 * This implementation of API limits stores
 * the counters inside memory, not in the databas
 * 
 * @author Pierre HUBERT
 */

import { time } from "../utils/DateUtils";
import { removeWhere } from "../utils/ArrayUtils";

// Different supported actions
export enum Action {
	LOGIN_FAILED = "login_failed",
	CREATE_ACCOUNT = "create_account"
}

/**
 * The duration entries will be kept in the table
 */
const MAX_TIME = 3600; // 1 hour

/**
 * Information about a specific IP & action
 */
interface CountInfo {
	action: Action,
	ip: string,
	time: number, // Begin time
	count: number,
}

// The list of limits
const list : Array<CountInfo> = [];

export class APILimitHelper {

	/**
	 * Find an entry in the list
	 * 
	 * If none is found, returns undefined if none found
	 * 
	 * @param ip The target IP address
	 * @param action Target action
	 */
	private static FindEntry(ip: string, action: Action) : CountInfo {

		// Remove old entries
		removeWhere(list, (entry) => entry.time + MAX_TIME < time());
		
		return list.find((f) => f.ip == ip && f.action == action);
	}

	/**
	 * Trigger the counter (increase it by one)
	 * 
	 * @param ip Target IP address
	 * @param action The action to check
	 */
	public static async Trigger(ip: string, action: Action) {
		const entry = this.FindEntry(ip, action);

		// Check if it is the first time the API is triggered
		if(entry == undefined)
			list.push({
				ip: ip,
				action: action,
				time: time(),
				count: 1
			})
		
		else
			entry.count++
	}

	/**
	 * Count the number of actions perfomed by a user
	 * 
	 * @param ip Target IP address
	 * @param action The action to check
	 */
	public static async Count(ip: string, action: Action) : Promise<number> {
		const entry = this.FindEntry(ip, action);
		
		return entry == undefined ? 0 : entry.count;
	}
}

