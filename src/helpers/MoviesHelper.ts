import { Movie } from "../entities/Movie";
import { DatabaseHelper } from "./DatabaseHelper";
import { PostsHelper } from "./PostsHelper";
import { existsSync, unlinkSync } from "fs";

/**
 * Legacy movies helper
 * 
 * @author Pierre HUBERT
 */

const MOVIES_TABLE = "galerie_video";

export class MoviesHelper {
	
	/**
	 * Get the list of movies of the user
	 * 
	 * @param userID The ID of the target user
	 */
	public static async GetListUser(userID: number) : Promise<Array<Movie>> {
		return (await DatabaseHelper.Query({
			table: MOVIES_TABLE,
			where: {
				ID_user: userID
			},
			order: "ID DESC"
		})).map((row) => this.DBToMovie(row));
	}

	/**
	 * Get information about a single movie
	 * 
	 * @param movieID The movie to get
	 */
	public static async GetInfo(movieID: number) : Promise<Movie> {
		const row = await DatabaseHelper.QueryRow({
			table: MOVIES_TABLE,
			where: {
				ID: movieID
			}
		});

		if(row == null)
			throw Error("Movie " + movieID + " could not be found!");
		
		return this.DBToMovie(row);
	}

	/**
	 * Check out whether a movie belongs to a user or not
	 * 
	 * @param userID Target user ID
	 * @param movieID The ID of the movie to check
	 */
	public static async DoesUserHas(userID: number, movieID: number): Promise<boolean> {
		try {
			return (await this.GetInfo(movieID)).userID == userID;
		} catch (error) {
			return false;
		}
	}

	/**
	 * Delete a movie created by the user
	 * 
	 * @param movie The movie to delete
	 */
	public static async Delete(movie: Movie) {
		
		// Delete all related posts
		await PostsHelper.DeleteAllWithMovie(movie);

		// Delete associated file
		if(existsSync(movie.sysPath))
			unlinkSync(movie.sysPath)

		// Remove movie from database
		await DatabaseHelper.DeleteRows(MOVIES_TABLE, {
			ID: movie.id
		})
	}

	/**
	 * Delete all the movies of a given user
	 * 
	 * @param userID The ID of the target user
	 */
	public static async DeleteAllUser(userID: number) {
		for(const m of await this.GetListUser(userID))
			await this.Delete(m);
	}

	/**
	 * Turn a database entry into a movie object
	 * 
	 * @param row Database entry
	 */
	private static DBToMovie(row: any) : Movie {
		return new Movie({
			id: row.ID,
			uri: row.URL,
			userID: row.ID_user,
			name: row.nom_video,
			fileType: row.file_type,
			size: Number(row.size)
		});
	}
}