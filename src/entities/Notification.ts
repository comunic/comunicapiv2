/**
 * Notification object
 * 
 * To avoid conflicts with the native Notification object, 
 * I renamed it Notif
 * 
 * @author Pierre HUBERT
 */

export enum NotifElemType {
	EMPTY = "",
	USER_PAGE = "user_page",
	GROUP_PAGE = "group_page",
	CONVERSATION = "conversation",
	CONVERSATION_MESSAGE = "conversation_message",
	POST = "post",
	POST_TEXT = "post_text",
	POST_IMAGE = "post_img",
	POST_YOUTUBE = "post_youtube",
	POST_MOVIE = "post_movie",
	POST_WEBLINK = "post_weblink",
	POST_PDF = "post_pdf",
	POST_TIMER = "post_timer",
	POST_SURVEY = "post_survey",
	COMMENT = "comment",
	FRIENDSHIP_REQUEST = "friend_request",
	GROUP_MEMBERSHIP = "group_membership",
}

export enum NotifEventType {
	COMMENT_CREATED = "comment_created",
	SENT_FRIEND_REQUEST = "sent_friend_request",
	ACCEPTED_FRIEND_REQUEST = "accepted_friend_request",
	REJECTED_FRIEND_REQUEST = "rejected_friend_request",
	ELEM_CREATED = "elem_created",
	ELEM_UPDATED = "elem_updated",
	SENT_GROUP_MEMBERSHIP_INVITATION = "sent_group_membership_invitation",
	ACCEPTED_GROUP_MEMBERSHIP_INVITATION = "accepted_group_membership_invitation",
	REJECTED_GROUP_MEMBERSHIP_INVITATION = "rejected_group_membership_invitation",
	SENT_GROUP_MEMBERSHIP_REQUEST = "sent_group_membership_request",
	ACCEPTED_GROUP_MEMBERSHIP_REQUEST = "accepted_group_membership_request",
	REJECTED_GROUP_MEMBERSHIP_REQUEST = "rejected_group_membership_request",
}

export enum NotifEventVisibility {
	EVENT_PRIVATE = "event_private",
	EVENT_PUBLIC = "event_public",
}

export interface NotifBuilder {
	id ?: number,
	timeCreate ?: number,
	seen ?: boolean,
	fromUserID ?: number,
	destUserID ?: number,
	onElemID ?: number,
	onElemType ?: NotifElemType,
	type ?: NotifEventType,
	eventVisibility ?: NotifEventVisibility,
	fromContainerID ?: number,
	fromContainerType ?: NotifElemType
}

export class Notif implements NotifBuilder {
	id ?: number;
	timeCreate ?: number;
	seen ?: boolean;
	fromUserID ?: number;
	destUserID ?: number;
	onElemID ?: number;
	onElemType ?: NotifElemType;
	type ?: NotifEventType;
	eventVisibility ?: NotifEventVisibility;
	fromContainerID ?: number;
	fromContainerType ?: NotifElemType;

	constructor(info: NotifBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) {
			 	this[key] = info[key];
			}
		}
	}

	get hasId() : boolean {
		return this.id > 0
	}

	get hasSeen() : boolean {
		return this.seen === true || this.seen === false
	}

	get hasFromUserID() : boolean {
		return this.fromUserID > 0
	}

	get hasDestUserID() : boolean {
		return this.destUserID > 0
	}

	get hasType() : boolean {
		return this.type != null && this.type != undefined
	}

	get hasOnElemID() : boolean {
		return this.onElemID > 0
	}

	get hasOnElemType() : boolean {
		return this.onElemType != null && this.onElemType != undefined
	}

	get hasTimeCreate() : boolean {
		return this.timeCreate != null
			&& this.timeCreate != undefined
			&& this.timeCreate > 0;
	}
}