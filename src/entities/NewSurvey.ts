/**
 * New survey information handler
 * 
 * @author Pierre HUBERT
 */

export interface NewSurveyBuilder {
	postID?: number,
	userID: number,
	question: string,
	choices: Array<string>,
	allowNewChoices: boolean
}

export class NewSurvey implements NewSurveyBuilder {
	postID: number;
	userID: number;
	question: string;
	choices: string[];
	allowNewChoices: boolean;

	public constructor(info: NewSurveyBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) 
				this[key] = info[key];
		}
	}
}