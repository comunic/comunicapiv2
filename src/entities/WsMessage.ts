/**
 * WebSocket message
 * 
 * @author Pierre Hubert
 */

export interface WsMessageBuilder {
	id: string,
	title: string,
	data: any
}

export class WsMessage implements WsMessageBuilder {
	id: string;
	title: string;
	data: any;
	
	public constructor(info: WsMessageBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}

	/**
	 * Construct quickly a message with no ID (to propagate
	 * events to clients)
	 * 
	 * @param title The title of message
	 * @param data Data associated with the message
	 */
	public static NoIDMessage(title: string, data: any) : WsMessage {
		return new this({
			id: "",
			title: title,
			data: data
		});
	}

	get isValidRequest() : boolean {
		return typeof this.id === "string"
			&& typeof this.title === "string"
			&& this.id.length > 0
			&& this.title.length > 0
			&& typeof this.data === "object";
	}
}