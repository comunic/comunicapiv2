/**
 * Group membership information
 * 
 * @author Pierre HUBERT
 */

export enum GroupMembershipLevels {
	ADMINISTRATOR = 0,
    MODERATOR = 1,
	MEMBER = 2,
	INVITED = 3,
	PENDING = 4, //When the group membership has not been approved yet
	VISITOR = 5, //Simple visitor
}

export interface GroupMemberConstructor {
	id: number,
	userID: number,
	groupID: number,
	timeCreate: number,
	level: GroupMembershipLevels,
	following: boolean
}

export class GroupMember implements GroupMemberConstructor {
	id: number;
	userID: number;
	groupID: number;
	timeCreate: number;
	level: GroupMembershipLevels;
	following: boolean;

	constructor(info: GroupMemberConstructor) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) {
				this[key] = info[key];
			}
		}
	}
}