/**
 * Single unread conversation information
 * 
 * @author Pierre HUBERT
 */

export interface UnreadConversation {
	id: number,
	name: string,
	lastActive: number,
	userID: number,
	message: string
}