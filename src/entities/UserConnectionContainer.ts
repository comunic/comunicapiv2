/**
 * Simple user connection container
 * 
 * @author Pierre Hubert
 */

export abstract class  AbstractUserConnectionContainer {
	public abstract getUserId() : number;
	public abstract get signedIn() : boolean;
}

/**
 * Fake user connnection checker used for notifications in WebSockets
 */
export class AbritraryUserConnection implements AbstractUserConnectionContainer {
	
	constructor(private userID: number) {}
	
	public getUserId(): number {
		if(!this.signedIn)
			throw new Error("User is not signed in!");

		return this.userID;
	}
	public get signedIn(): boolean {
		return this.userID > 0;
	}

}