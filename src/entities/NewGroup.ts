/**
 * New group information
 * 
 * @author Pierre HUBERT
 */

export interface NewGroup {
	
	/**
	 * The name of the group to create
	 */
	name: string,

	/**
	 * The ID of the user creating the group
	 */
	userID: number,

	/**
	 * The time of creation of the group
	 */
	timeCreate: number
}