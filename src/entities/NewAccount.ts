/**
 * New account information
 * 
 * @author Pierre HUBERT
 */

export interface NewAccount {
	firstName: string,
	lastName: string,
	email: string,
	password: string
}