import { pathUserData } from "../utils/UserDataUtils";

/**
 * Single comment information
 * 
 * @author Pierre HUBERT
 */

export interface CommentBuilder {
	id: number,
	timeSent: number,
	userID: number,
	postID: number,
	content: string,
	imagePath ?: string,
}

export class Comment implements CommentBuilder {
	id: number;	timeSent: number;
	userID: number;
	postID: number;
	content: string;
	imagePath?: string;

	public constructor(info: CommentBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}

	get hasImage() : boolean {
		return this.imagePath != null && this.imagePath != "" && this.imagePath != "null";
	}

	get imageURL() : string {
		return pathUserData(this.imagePath);
	}

	get imageSysPath() : string {
		return pathUserData(this.imagePath, true);
	}
}