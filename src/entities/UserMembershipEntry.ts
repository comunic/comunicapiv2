import { Friend } from "./Friend";
import { Conversation } from "./Conversation";

/**
 * Get information about a membership
 * 
 * @author Pierre HUBERT
 */

export enum UserMembershipType { GROUP, FRIEND, CONVERSATION }

export class UserMembershipEntry {
	constructor(
		public lastActivity: number,
		public el: Friend | Conversation | number,
		public type: UserMembershipType
	) {}
}