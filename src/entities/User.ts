/**
 * User information
 * 
 * @author Pierre HUBERT
 */

import { pathUserData } from "../utils/UserDataUtils";

export const SupportedLanguages = ["fr", "en"]

export enum AccountImageVisibilityLevel {
	FRIENDS = "friends",
	COMUNIC_USERS = "comunic_users",
	EVERYONE = "everyone"
}

const defaultAccountImage = "avatars/0Reverse.png";
const errorAccountImage = "avatars/0Red.png";


export enum UserPageStatus {
	PRIVATE,
	PUBLIC,
	OPEN
}

export interface UserInfo  {
	id: number,
	firstName: string,
	lastName: string,
	timeCreate: number,
	virtualDirectory: string,
	pageStatus: UserPageStatus,
	accountImagePath: string,
	accountImageVisibilityLevel: AccountImageVisibilityLevel,
	friendsListPublic: boolean,
	personnalWebsite ?: string,
	publicNote ?: string,
	blockComments : boolean,
	allowPostsFromFriends: boolean,
}

export interface GeneralSettings {
	id: number,
	email ?: string,
	firstName: string,
	lastName: string,
	pageStatus: UserPageStatus,
	blockComments: boolean,
	allowPostsFromFriends: boolean,
	friendsListPublic: boolean,
	personnalWebsite ?: string,
	virtualDirectory: string,
	allowMails: boolean,
	publicNote ?: string
}

export interface LangSettings {
	id: number,
	lang: string,
}

export interface SecuritySettings {
	id: number,
	security_question_1 ?: string,
	security_answer_1 ?: string,
	security_question_2 ?: string,
	security_answer_2 ?: string
}


export interface UserBuilder extends UserInfo, SecuritySettings, LangSettings, GeneralSettings {

}

export class User implements UserBuilder {
	id: number;
	email: string;
	firstName: string;
	lastName: string;
	timeCreate: number;
	virtualDirectory: string;
	pageStatus: UserPageStatus;
	friendsListPublic: boolean;
	personnalWebsite?: string;
	publicNote?: string;
	blockComments: boolean;
	allowPostsFromFriends: boolean;
	allowMails: boolean;
	lang: string;
	accountImagePath: string;
	accountImageVisibilityLevel: AccountImageVisibilityLevel;
	security_question_1?: string;
	security_answer_1?: string;
	security_question_2?: string;
	security_answer_2?: string;
	

	public constructor(info : UserBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) {
				this[key] = info[key];
			}
		}
	}
	

	/**
	 * Get account image URL
	 */
	get accountImageURL() : string {
		if(this.accountImagePath.length < 1)
			return User.pathForAccountImageFile(defaultAccountImage);
		
		return User.pathForAccountImageFile(this.accountImagePath);
	}

	/**
	 * Get account image sys path
	 */
	get accountImageSysPath() : string {
		if(this.accountImagePath.length < 1)
			throw new Error("This user has no account image!");
		
		return User.pathForAccountImageFile(this.accountImagePath, true);
	}

	/**
	 * Check out whether the user has an account image
	 * or if it is the default account image
	 */
	get hasAccountImage() : boolean {
		return this.accountImagePath.length > 0;
	}

	/**
	 * Fallback account image URL
	 */
	static get errorAccountImageURL() : string {
		return this.pathForAccountImageFile(errorAccountImage);
	}

	private static pathForAccountImageFile(file : string, sysPath = false) : string {
		return pathUserData(file, sysPath);
	}
	

	get isPublic() : boolean {
		return this.pageStatus != UserPageStatus.PRIVATE;
	}

	get isOpen() : boolean {
		return this.pageStatus == UserPageStatus.OPEN;
	}

	get hasVirtualDirectory() : boolean {
		return this.virtualDirectory != null 
			&& this.virtualDirectory != "null"
			&& this.virtualDirectory.length > 0
	}

	get hasWebsite() : boolean {
		return this.personnalWebsite 
			&& this.personnalWebsite != null 
			&& this.personnalWebsite.length > 0 
			&& this.personnalWebsite != "null"
	}

	get hasPublicNote() : boolean {
		return this.publicNote 
			&& this.publicNote != null 
			&& this.publicNote.length > 0 
			&& this.publicNote != "null"
	}

	get hasSecurityQuestion1() : boolean {
		return this.security_question_1 != null && this.security_question_1 &&  this.security_question_1.length > 0;
	}


	get hasSecurityAnswer1() : boolean {
		return this.security_answer_1 != null && this.security_answer_1 &&  this.security_answer_1.length > 0;
	}

	get hasSecurityQuestion2() : boolean {
		return this.security_question_2 != null && this.security_question_2 != null && this.security_question_2 && this.security_question_2.length > 0;
	}


	get hasSecurityAnswer2() : boolean {
		return this.security_answer_2 != null && this.security_answer_2 &&  this.security_answer_2.length > 0;
	}

	get hasSecurityQuestions() : boolean {
		return (this.hasSecurityQuestion1 && this.hasSecurityAnswer1 
			&& this.hasSecurityQuestion2 && this.hasSecurityAnswer2) == true;
	}
}