import { pathUserData } from "../utils/UserDataUtils";

/**
 * Single conversation message
 * 
 * @author Pierre HUBERT
 */

// Used directly when creating a new message
export interface BaseConversationMessage {
	convID: number,
	userID: number,
	imagePath: string,
	message: string
}

export interface ConversationMessageInfo extends BaseConversationMessage {
	id: number,
	timeSent: number,
}

export class ConversationMessage implements ConversationMessageInfo {
	public id: number;
	public convID: number;
	public userID: number;
	public timeSent: number;
	public imagePath: string;
	public message: string;

	constructor(info: ConversationMessageInfo) {
		this.id = info.id;
		this.convID = info.convID;
		this.userID = info.userID;
		this.timeSent = info.timeSent;
		this.imagePath = info.imagePath;
		this.message = info.message;
	}

	get hasImage() : boolean {
		return this.imagePath.length > 1;
	}

	get imageURL() : string {
		return pathUserData(this.imagePath);
	}

	get imageSysPath() : string {
		return pathUserData(this.imagePath, true);
	}
}