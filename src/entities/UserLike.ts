/**
 * User like
 * 
 * @author Pierre Hubert
 */

export interface UserLikeBuilder {
	id: number,
	userID: number,
	timeSent: number,
	elemType: string,
	elemId: string
}

export class UserLike implements UserLikeBuilder {
	id: number;
	userID: number;
	timeSent: number;
	elemType: string;
	elemId: string;

	public constructor(info: UserLikeBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}
}