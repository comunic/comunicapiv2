/**
 * Information about an API client
 * 
 * @author Pierre HUBERT
 */

export interface APIClient {
	id: number,
	name: string,
	token: string,
	domain: string,
}