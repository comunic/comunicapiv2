import { pathUserData } from "../utils/UserDataUtils";
import { join } from "path";

/**
 * Single group information
 * 
 * @author Pierre HUBERT
 */

export const PATH_GROUP_LOGO = "groups_logo";

/**
 * Group visibility level
 */
export enum GroupVisibilityLevel {
	OPEN_GROUP = 0,
	PRIVATE_GROUP = 1,
	SECRETE_GROUP = 2
}

/**
 * Access level of a user to a group
 */
export enum GroupsAccessLevel {
	NO_ACCESS = 0, //Can not even know if the group exists or not
	LIMITED_ACCESS = 1, //Access to the name of the group only
	VIEW_ACCESS = 2, //Can see the posts of the group, but not a member of the group
	MEMBER_ACCESS = 3, //Member access (same as view access but as member)
	MODERATOR_ACCESS = 4, //Can create posts, even if posts creation is restricted
	ADMIN_ACCESS = 5, //Can do everything
}

/**
 * Registration level of groups
 */
export enum GroupRegistrationLevel {
	OPEN_REGISTRATION = 0,
	MODERATED_REGISTRATION = 1,
	CLOSED_REGISTRATION = 2,
}

export enum GroupPostsCreationLevel {
	POSTS_LEVEL_MODERATORS = 0, //Only the moderators and the administrator can create posts
	POSTS_LEVEL_ALL_MEMBERS = 1, //All the members of the group can create posts
}

export interface GroupInfoConstructor {
	id: number,
	name: string,
	membersCount: number,
	visiblity: GroupVisibilityLevel,
	registrationLevel: GroupRegistrationLevel,
	postsCreationLevel: GroupPostsCreationLevel,
	logo ?: string,
	virtualDirectory ?: string,
	timeCreate: number,
	description ?: string,
	url ?: string,
}

export class GroupInfo implements GroupInfoConstructor {
	timeCreate: number;
	description?: string;
	url?: string;
	id: number;
	name: string;
	membersCount: number;
	visiblity: GroupVisibilityLevel;
	registrationLevel: GroupRegistrationLevel;
	postsCreationLevel: GroupPostsCreationLevel;
	logo?: string;
	virtualDirectory?: string;

	constructor(info: GroupInfoConstructor) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) {
				this[key] = info[key];
			}
		}
	}

	get hasLogo() : boolean {
		return this.logo 
			&& this.logo != "null" 
			&& this.logo != undefined;
	}

	get logoPath() : string {
		if(this.hasLogo)
			return this.logo
		else
			return join(PATH_GROUP_LOGO, "default.png");
	}

	get logoURL() : string {
		return pathUserData(this.logoPath, false);
	}

	get logoSysPath() : string {
		if(!this.hasLogo)
			throw Error("The group has no logo!");

		return pathUserData(this.logoPath, true);
	}

	get hasDescription() : boolean {
		return this.description && true;
	}

	get hasURL() : boolean {
		return this.url && true;
	}
}