/**
 * Response to a survey
 * 
 * @author Pierre HUBERT
 */

export interface SurveyResponseBuilder {
	id: number,
	timeSent: number,
	userID: number,
	surveyID: number,
	choiceID: number
}

export class SurveyResponse implements SurveyResponseBuilder {
	id: number;
	timeSent: number;
	userID: number;
	surveyID: number;
	choiceID: number;
	
	public constructor(info: SurveyResponseBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}
}