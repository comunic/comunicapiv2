/**
 * Custom smiley
 * 
 * @author Pierre Hubert
 */

import { pathUserData } from "../utils/UserDataUtils";

export interface CustomEmojiBuilder {
	id: number,
	userID: number,
	shortcut: string,
	path: string,
}

export class CustomEmoji implements CustomEmojiBuilder {
	id: number;
	userID: number;
	shortcut: string;
	path: string;

	public constructor(info: CustomEmojiBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}

	get url() : string {
		return pathUserData(this.path, false);
	}

	get sysPath() : string {
		return pathUserData(this.path, true);
	}
}