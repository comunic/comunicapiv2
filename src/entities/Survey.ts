/**
 * Survey information
 * 
 * @author Pierre HUBERT
 */

export interface SurveyChoice {
	id: number,
	name: string,
	count: number
}

export interface SurveyBuilder {
	id: number;
	userID: number;
	timeCreate: number;
	postID: number;
	question: string;
	choices: SurveyChoice[];
	allowNewChoices: boolean;
}

export class Survey implements SurveyBuilder {
	id: number;	userID: number;
	timeCreate: number;
	postID: number;
	question: string;
	choices: SurveyChoice[];
	allowNewChoices: boolean;

	public constructor(info: SurveyBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}
}