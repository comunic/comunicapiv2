/**
 * Single conversation information
 * 
 * @author Pierre HUBERT
 */

export interface BaseConversation {
	ownerID: number,
	name: string,
	following: boolean,
	members: Set<number>,
	canEveryoneAddMembers: boolean,
}

export interface Conversation extends BaseConversation {
	id: number,
	lastActive: number,
	timeCreate: number,
	sawLastMessage: boolean,
	canEveryoneAddMembers: boolean,
}