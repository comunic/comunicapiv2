/**
 * User tokens
 * 
 * @author Pierre HUBERT
 */

export interface UserLoginTokens {
	userID : number,
	clientID : number,
	token1: string,
	token2: string,
}