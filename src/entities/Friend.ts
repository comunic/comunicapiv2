/**
 * Friend information
 * 
 * @author Pierre HUBERT
 */

export interface FriendBuilder {
	friendID: number,
	accepted: boolean,
	following: boolean,
	lastActivityTime: number,
	canPostTexts: boolean
}

export class Friend implements FriendBuilder {
	friendID: number;	accepted: boolean;
	following: boolean;
	lastActivityTime: number;
	canPostTexts: boolean;

	public constructor(info: FriendBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}
}