import { pathUserData } from "../utils/UserDataUtils";

/**
 * Post entity
 * 
 * @author Pierre HUBERT
 */

export enum PostVisibilityLevel {
	//Posts that can be seen by anyone
	VISIBILITY_PUBLIC = 1,

	//Posts that can be seen by the friends of the user
	VISIBILITY_FRIENDS = 2,

	//Posts that can be seen by the user only
	VISIBILITY_USER = 3,

	//Posts that can be seen by the members of a group (same as friends)
	VISIBILITY_GROUP_MEMBERS = 50,
}


export enum PostAccessLevel {
	//When a user can't access to a post
	NO_ACCESS = 0,

	//When a user can see a post and perform basic actions such as liking
	BASIC_ACCESS = 1,

	//When a user has intermediate access to the post (delete post)
	INTERMEDIATE_ACCESS = 2,

	//When a user has a full access to the post
	FULL_ACCESS = 3,
}

export enum PostKind {
	POST_KIND_TEXT = "text",
	POST_KIND_IMAGE = "image",
	POST_KIND_WEBLINK = "weblink",
	POST_KIND_PDF = "pdf",
	POST_KIND_MOVIE = "movie",
	POST_KIND_COUNTDOWN = "countdown",
	POST_KIND_SURVEY = "survey",
	POST_KIND_YOUTUBE = "youtube",
}

export enum PostPageKind {
	PAGE_KIND_USER = "user",
	PAGE_KIND_GROUP = "group",
}


export interface PostFileBuilder {
	path: string,
	size: number,
	type: string,
}

export class PostFile implements PostFileBuilder {
	path: string;
	size: number;
	type: string;

	public constructor(info: PostFileBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) 
				this[key] = info[key];
		}
	}

	get url() : string {
		return pathUserData(this.path)
	}

	get sysPath() : string {
		return pathUserData(this.path, true);
	}
}

export interface PostLinkBuilder {
	url: string,
	title: string,
	description: string,
	image: string
}

export class PostLink implements PostLinkBuilder {
	url: string;
	title: string;
	description: string;
	image: string;

	public constructor(info: PostLinkBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) 
				this[key] = info[key];
		}
	}
}


export interface PostBuilder {
	id: number,
	userID: number,
	timeCreate: number,
	kindPage: PostPageKind,
	pageID: number,
	content: string,
	visibilityLevel: PostVisibilityLevel,
	kind: PostKind,
	file ?: PostFile,
	movieID ?: number,
	timeEnd ?: number,
	link ?: PostLink,
}

export class Post implements PostBuilder {
	id: number;	
	userID: number;
	timeCreate: number;
	kindPage: PostPageKind;
	pageID: number;
	content: string;
	visibilityLevel: PostVisibilityLevel;
	kind: PostKind;
	file?: PostFile;
	movieID?: number;
	timeEnd?: number;
	link?: PostLink;

	public constructor(info: PostBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key)) 
				this[key] = info[key];
		}
	}

	get isUserPage() : boolean {
		return this.kindPage == PostPageKind.PAGE_KIND_USER;
	}

	get userPageID() : number {
		return this.isUserPage ? this.pageID : 0
	}

	get groupID() : number {
		return this.kindPage == PostPageKind.PAGE_KIND_GROUP ? this.pageID : 0
	}

	get hasContent() : boolean {
		return this.content != null && this.content && this.content.length > 0;
	}

	get hasFile() : boolean {
		return this.file != null && this.file != undefined;
	}

	get hasMovie() : boolean {
		return this.kind == PostKind.POST_KIND_MOVIE;
	}

	get hasTimeEnd() : boolean {
		return this.kind == PostKind.POST_KIND_COUNTDOWN;
	}

	get hasLink() : boolean {
		return this.kind == PostKind.POST_KIND_WEBLINK;
	}

	get hasSurvey() : boolean {
		return this.kind == PostKind.POST_KIND_SURVEY;
	}
}