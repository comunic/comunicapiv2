import { pathUserData } from "../utils/UserDataUtils";

/**
 * Movie entity
 * 
 * @author Pierre HUBERT
 */

export interface MovieBuilder {
	id: number,
	userID: number,
	name: string,
	uri: string,
	fileType: string,
	size: number
}

export class Movie implements MovieBuilder {
	id: number;	userID: number;
	name: string;
	uri: string;
	fileType: string;
	size: number;

	public constructor(info: MovieBuilder) {
		for (const key in info) {
			if (info.hasOwnProperty(key))
				this[key] = info[key];
		}
	}

	get url() : string {
		return pathUserData(this.uri);
	}

	get sysPath() : string {
		return pathUserData(this.uri, true);
	}
}