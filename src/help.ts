/**
 * Command-line usage help
 * 
 * @author Pierre Hubert
 */

export function showHelp() {
	console.info(
		"Usage:\n" +
		"* server (default) : Start the API server\n" +
		"* help : Show this message\n" +
		"* migration [mig_id] : Start a migration"
	);

	process.exit(0);
}