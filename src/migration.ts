/**
 * Migration assistant
 * 
 * @author Pierre Hubert
 */

import { MigrationsList } from "./migrations/MigrationsList";
import { createInterface } from "readline";


function fatal(msg : string) {
	console.error("Fatal error: " + msg);
	process.exit(-1);
}

/**
 * Start a new migration
 * 
 * @param args Arguments passed to the command line
 */
export async function startMigration(args: String[]) {
	if(args.length < 1)
		fatal("Missing migration ID!");

	const migrationID = args[0];
	const migration = MigrationsList.find((m) => m.id == migrationID);

	if(!migration)
		fatal("Could not find requested migration: "+migrationID+" !");
	
	// Ask user confirmation before continuing
	const rl = createInterface({
		input: process.stdin,
		output: process.stdout
	});

	const response = await new Promise((res, _rej) => rl.question(
		"Do you want to start this migration: " + migration.id + " ? (y/n) ", 
		(ans) => res(ans)
	));

	rl.close();

	if(response !== "y")
		fatal("Operation aborted.");
	
	// Do the migration
	await migration.func();

	console.info("Finished.")

	process.exit(0);
}