/**
 * Array utilities
 * 
 * @author Pierre HUBERT
 */

/**
 * Find the key matching a given value in an object
 * 
 * @param object Object to search in
 * @param value The value to search for
 * @returns Matching key, or null if not found
 */
export function findKey(object: Object, value: any): string {
	for (const key in object) {
		if (!object.hasOwnProperty(key))
			continue;
		
		if(object[key] == value)
			return key;
	}

	return null;
}

/**
 * Remove all the entries of an array that meets a certain condition
 * 
 * @param array Input array
 * @param callback Callback function to call on each element
 * of the array
 */
export function removeWhere<T>(array: Array<T>, callback: (el: T) => boolean) {
    var i = array.length;
    while (i--) {
        if (callback(array[i])) {
            array.splice(i, 1);
        }
    }
};