/**
 * Date utilities
 * 
 * @author Pierre HUBERT
 */

export function time() : number {
	return Math.floor((new Date()).getTime()/1000);
}


/**
 * Get the current date formatted for the "DATETIME" object of 
 * a MySQL database
 */
export function mysql_date() : string {
	const date = new Date();

	return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + (date.getDate())
		+ " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}