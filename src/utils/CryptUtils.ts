/**
 * Cryptographic utilities
 * 
 * @author Pierre HUBERT
 */

import * as crypto from 'crypto';

import { execSync } from 'child_process';

/**
 * Generate sha1-encoded string
 * 
 * @param str The string to encode
 */
export function sha1(str : string) : string {
	const shasum = crypto.createHash("sha1");
	shasum.update(str);
	return shasum.digest("hex");
}

/**
 * Interface with PHP crypt function
 * 
 * @param str The string to encrypt
 * @param salt Salt to use
 */
export function crypt(str : string, salt: string) : string {
	return execSync("/usr/bin/php -r \"echo crypt(\'"+str+"\', \'"+salt+"\');\"").toString();
}

/**
 * Generate a random integer
 * 
 * @param max Maximum value to except
 */
export function randInt(max: number) : number {
	return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Generate a random string
 * 
 * @param length The length of the string to generate
 * @param keyspace Keyspace to use
 */
export function randomStr(length: number, keyspace : string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") : string {
	let str = "";

	for (let i = 0; i < length; i++) {
		str += keyspace[randInt(keyspace.length)];
	}

	return str;
}