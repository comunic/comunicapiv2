import { AccountHelper } from "../helpers/AccountHelper";
import { GroupsHelper } from "../helpers/GroupsHelper";

/**
 * Virtual directories utilities
 * 
 * @author Pierre HUBERT
 */

export enum VirtualDirType {
	USER,
	GROUP
}

/**
 * Check out whether a virtual directory is valid or not
 * 
 * @param dir  The virtual directory to check
 */
export function checkVirtualDirectory(dir: string) : boolean {
	if(dir.length < 4 || dir.length > 30) return false;

	for(let el of [".html", ".txt", ".php", "à", "â", "é", "ê", "@", "/", "\"", "'", '"', "<", ">", "?", "&", "#"])
		if(dir.includes(el))
			return false;
		
	return true;
}

/**
 * Check the availability of a virtual directory
 * 
 * @param dir Directory to check
 * @param id The ID of the element to check
 * @param type The type of the element
 */
export async function checkVirtualDirectoryAvailability(dir: string, id: number, type: VirtualDirType) : Promise<boolean> {
	if(!checkVirtualDirectory(dir)) return false;

	if(type == VirtualDirType.USER) {
		return await AccountHelper.CheckUserDirectoryAvailability(dir, id) 
			&& await GroupsHelper.CheckDirectoryAvailability(dir);
	}

	else {
		return await AccountHelper.CheckUserDirectoryAvailability(dir)
			&& await GroupsHelper.CheckDirectoryAvailability(dir, id);
	}
}