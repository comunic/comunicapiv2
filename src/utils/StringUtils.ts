/**
 * String utilities
 * 
 * @author Pierre HUBERT
 */

/**
 * Check a given email address
 * 
 * @param {String} emailAddress The email address to check
 * @return {Boolean} True for a valid email address / false else
 */
export function checkMail(emailAddress: string): boolean {
	return (emailAddress.match(/^[a-zA-Z0-9_.]+@[a-zA-Z0-9-.]{1,}[.][a-zA-Z]{2,8}$/) === null ? false : true);
}

/**
 * Check a URL validity
 * 
 * Source: https://gist.github.com/729294
 * 
 * @param {string} url The URL to check
 * @return {boolean} TRUE if the URL is valid
 */
export function checkURL(url: string) : boolean {
	const regex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i

	return url.match(regex) == null ? false : true;
}

/**
 * Fix text encoding
 * 
 * @param input Input string
 */
export function fixEncoding(input : string) : string {
	return Buffer.from(input, "latin1").toString("utf-8");
}

/**
 * Remove HTML markup codes in a string (<, >)
 * 
 * @param input String to change
 */
export function removeHTMLNodes(input : string) : string {
	return input.replace(/</g, "&lt;")
				.replace(/>/g, "&gt;");
}

/**
 * Check a string before inserting it
 * 
 * Legacy function that might be completed
 *  / replaced in the future
 * 
 * @param s The string to check
 */
export function check_string_before_insert(s: string) : boolean {
	return s.trim().length >= 3;
}


/**
 * Check the validity of a YouTube ID
 * 
 * @param s The id to check
 */
export function check_youtube_id(s: string) : boolean {
	return s.length >= 5
		&& !s.includes("/")
		&& !s.includes("\\")
		&& !s.includes("@")
		&& !s.includes("&")
		&& !s.includes("?")
		&& !s.includes(".")
		&& !s.includes("'")
		&& !s.includes("\"")
}

/**
 * Check an emoji code
 * 
 * @param s The emoji code to check
 */
export function checkEmojiCode(s: string) : boolean {
	return s.match(/^:[a-zA-Z0-9]+:$/) != null
}