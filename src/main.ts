import { ConfigurationHelper } from "./helpers/ConfigHelper";
import { DatabaseHelper } from "./helpers/DatabaseHelper";
import { startServer } from "./server";
import { showHelp } from "./help";
import { startMigration } from "./migration";

/**
 * Main project script
 * 
 * @author Pierre HUBERT
 */

console.info("Comunic API v6\t@author Pierre HUBERT\t2019-" + new Date().getFullYear());

async function init() {
	
	const confFile = process.argv.length < 3 ? "config.json" : process.argv[2];

	console.info("Load configuration from "+confFile+"...");
	ConfigurationHelper.loadConf(confFile);

	console.info("Connect to database");
	await DatabaseHelper.connect();

	const action = process.argv.length < 4 ? "server" : process.argv[3];

	switch(action) {
		case "help":
			showHelp();
			break;
		
		case "server":
			await startServer();
			break;
		
		case "migration":
			await startMigration(process.argv.slice(4));
			break;
		
		
	}
}
init();